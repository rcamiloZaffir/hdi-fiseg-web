#!groovy?
pipeline {

    agent { label 'node-agent'}

    tools {
        nodejs 'NodeJS-latest'
    }
    environment {
        EMAIL_BODY_BUILD = "Job status "+" - \"${env.JOB_NAME}\" \nBranch: ${env.GIT_BRANCH} \nbuild: ${env.BUILD_NUMBER}\n\nView the log at:\n ${env.BUILD_URL}"
        COMMITER_EMAIL= "devops-iw@interware.com.mx"
        DOCKER_IMAGE="dockerhub:5000/${env.JOB_NAME}".toLowerCase()
        DOCKER_NAME="${env.JOB_NAME}".toLowerCase()
        DOCKER_VERSION = getVersion("frontend/package.json")
        APP_NAME="${env.JOB_NAME}".toLowerCase()
        SONAR_PROJECT="hdi-fiseg-web"
        SERVICE_IP="dev.interware.com.mx"
        SERVICE_PORT=5000
        DOCKER_REGISTRY="dockerhub:5000"
    }
    
    stages {
        stage('Build') {
            steps {
                script {
                    sh 'env'
                    COMMITER_EMAIL = sh (
                            script: 'git --no-pager show -s --format=\'%ae\'',
                            returnStdout: true
                    ).trim()   
                    sh 'cd frontend; npm install'                 
                }
            }
        }
        
        stage('test') {
            steps {
                sh 'export CI=true; cd frontend; npm run test -- --coverage'
            }
        }

        stage("build & SonarQube analysis") {
            steps{
                withSonarQubeEnv('sonarqube') {
                    sh "cd frontend; npm run test:sonar -Dsonar.projectKey=${SONAR_PROJECT} -Dsonar.projectName=${SONAR_PROJECT}"
                }
            }
        }

        stage("Quality Gate"){
            steps {
                script {
                    try{
                        timeout(time: 1, unit: 'HOURS') {
                            def qg = waitForQualityGate()                        
                            if (qg.status != 'OK') {
                                sendEmail("Pipeline aborted due to quality gate failure: ${qg.status} - ${EMAIL_BODY_BUILD}".replace("status", "FAILED"))
                                error "Pipeline aborted due to quality gate failure: ${qg.status}"
                                }
                            }
                    }catch(e){
                        echo "Error al revisar el QualityGate ${e}"
                    }
                }                    
            }
        }

        stage('package') {
            steps {
                // sh "cd frontend; npm run build"
                sh "docker build -t ${DOCKER_IMAGE}:${DOCKER_VERSION} -f docker/Dockerfile ."
            }
        }
        stage('Publish artifact') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'nexusAdmin', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                    sh "docker login -u=$USERNAME -p=$PASSWORD dockerhub:5000"
                    sh "cd docker; docker push ${DOCKER_IMAGE}:${DOCKER_VERSION}"
                }   
            }
        }

        stage('Deploy') {
            steps {
                script {
                  sh "sed -i 's/.*image:.*/    image: ${DOCKER_REGISTRY}\\/${DOCKER_NAME}:${DOCKER_VERSION}/' docker/docker-compose.yml"
                    DOCKER_COMPOSE = sh (
                            script: 'base64 docker/docker-compose.yml | tr -d "\n"',
                            returnStdout: true
                    ).trim()
                    ansibleDeploy(buildExtraVars("${APP_NAME}","install_service", "docker_compose", DOCKER_COMPOSE), 'docker_operations', 'docker_swarm')
                }
            }
        }
        stage('Check app') {
            steps {
                script {
                        sh "sleep 50"
                        sh "curl http://${SERVICE_IP}:${SERVICE_PORT}/login"
                }
            }
        }        

    }

    post {
        always {
            echo 'JENKINS PIPELINE EXECUTION FINISH'
        }
        success {
            echo 'JENKINS PIPELINE HAS BEEN EXECUTED SUCCESSFULLY'
            sendEmail("${EMAIL_BODY_BUILD}".replace("status", "SUCCESS"))
        }
        failure {
            echo 'JENKINS PIPELINE HAS BEEN FAILED'
            sendEmail("${EMAIL_BODY_BUILD}".replace("status", "FAILED"))
        }
        aborted {
            echo 'JENKINS PIPELINE HAS BEEN ABORTED BY SYSTEM'
            sendEmail("Deploy aborted BY SYSTEM ${EMAIL_BODY_BUILD}".replace("status", "ABORTED"))
        }
    }

}


def getVersion(json){
    def packageJSON = readJSON file: json
    def packageJSONVersion = packageJSON.version
    packageJSONVersion
}

def sendEmail(String body){
    script {
        echo "Sending mail to: " + "${COMMITER_EMAIL}"
        mail    to: "${COMMITER_EMAIL}",
                subject: "JENKINS Build: \"${env.JOB_NAME}\" - ",
                body: "${body}"
        echo "Finish sending email."
        echo 'Clean Workspace'
    }
}

def buildExtraVars(String stack, String operation , String type, String docker_compose ){
    String extraVars = """--- 
                        stack: "${stack}"
                        operation: "${operation}"
                        type: "${type}"
                        docker_compose: "${docker_compose}"
                        """
    extraVars
}

def ansibleDeploy(String extraVars,
                  String jobTemplate,
                  String limit){
    println "Executing ansible playbook."
    echo extraVars
    result = ansibleTower(
            async: false,
            credential: '',
            extraVars: "${extraVars}",
            importTowerLogs: true,
            importWorkflowChildLogs: false,
            inventory: '',
            jobTags: '',
            jobTemplate: "${jobTemplate}",
            jobType: 'run',
            limit: "${limit}",
            removeColor: false,
            skipJobTags: '',
            templateType: 'job',
            throwExceptionWhenFail: false,
            towerCredentialsId: 'ansible_tower',
            towerServer: 'Ansible Tower Demo',
            verbose: false)
    println "Finish ansible playbook." + result
    if (result['JOB_RESULT'] != 'SUCCESS') {
        error "Pipeline aborted, because deploy fail"
    }
}   