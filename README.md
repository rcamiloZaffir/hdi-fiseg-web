# HDI Seguros

### To start (frontend and proxy)

```bash
cd frontend
npm i

# proxy
npm run server
# webpage
npm start
```

### To start (ENV) .

```bash

# HDI
export API=http://198.199.117.133:8096
export API_PROXY_SECURITY_SERVICE=http://proxysecurityservice.ts02.hdi.net:8032/api/Seguridad
export API_SERVICE_WRAPPER=http://servicewrapper.ts07.hdi.net:8000/api/Security
export PORTAL_ROLE="DAN - AGENTE"
export REACT_APP_SEND_MAIL_ACTIVE_FE=true
export REACT_APP_ENV=production

```

### To start (mock-service)

```bash
cd mock-services
npm i
npm start
```

Open [localhost:3001](http://localhost:3001) to view it in the browser.

### To test (frontend)

```bash
cd frontend
npm i
npm test
```

### To build (frontend)

```bash
cd frontend
npm i
npm run build
```

### To deploy (frontend)

```bash
npm i
npm run server
```

## Instalación en docker swarm

Para instalar el servicio, es necesario realizar los siguientes pasos:

### Crear red

Para verificar que exista:

```bash
docker network ls | grep hdi_fiseg_data_net
```

Para crear la red:

```bash
docker network create -d overlay hdi_fiseg_data_net
```

### Construir imagen docker

```bash
npm i --only=prod
npm run build
docker build -t dockerhub:5000/hdi-fiseg-web:$(egrep \"version\" frontend/package.json | awk -F"\"" '{print $4}') -f docker/Dockerfile .
```

Donde:

- $(egrep \"version\" frontend/package.json | awk -F"\"" '{print $4}'): Obtiene la versión que esta en el archivo **frontend/package.json**

### Correr docker container en docker swarm(docker-compose)

Substituir la definición del docker compose:

```bash
version: "3"

services:
  web:
    image: dockerhub:5000/hdi-fiseg-web:0.0.0
    restart: always
    ports:
      - 5000:5000
    environment:
      - API=http://dev.interware.com.mx:8093
    networks:
      - hdi_fiseg_net

networks:
  hdi_fiseg_net:
    external:
      name: hdi_fiseg_data_net
```

**Campos a actualizar:**

- **image**: Actualizar **dockerhub:5000** con el hostname y puerto del docker registry. También actualizar la **versión** que deseamos instalar
- **ports**: Si deseamos que nuestro servicio se exponga en otro puerto cambiar el puerto del lazo izquierdo
- **API**: **URL** del servicio **hdi-fiseg-support**

Ejecutar el siguiente comando:

```bash
docker stack deploy --prune --with-registry-auth --resolve-image always --compose-file=docker/docker-compose.yml hdi-fiseg-web
```

### Correr docker container en docker swarm(docker service)

```bash
docker service create  \
--name hdi-fiseg-web \
--env API=http://dev.interware.com.mx:8093 \
--publish 5000:5000 \
dockerhub:5000/hdi-fiseg-web:0.0.0

```

**Donde:**

- **image**: **dockerhub:5000** es el hostname y puerto del docker registry.
- **0.0.0** Es la **versión** que deseamos instalar
- **API**: Es la variable de ambiente que contiene la **URL** del servicio **hdi-fiseg-support**
- **publish**: Si deseamos que nuestro servicio se exponga en otro puerto cambiar el puerto del lazo izquierdo
