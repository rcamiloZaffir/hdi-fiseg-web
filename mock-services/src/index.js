const responseJson = require('./response.json')
const jwt = require('jsonwebtoken')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const PORT = 5001
const axios = require('axios')

const {
  API_PROXY_SECURITY_SERVICE,
  API_SERVICE_WRAPPER,
  PORTAL_ROLE,
} = process.env

app.use(
  cors({
    origin: [
      'http://localhost:3000',
      'http://localhost:8080',
      'http://localhost:3001',
      'http://localhost:5000',
      'http://198.199.109.168:5000',
      'http://dev.interware.com.mx:5000',
    ],
    credentials: true,
  })
)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json({ limit: '50mb', extended: true }))

app.post('/jwt/login', async (req, res) => {
  const { username, password } = req.body

  const tokenAutentificacion = await axios.post(
    `${API_PROXY_SECURITY_SERVICE}/TokenAutentificacion`,
    {
      usuario: username,
      usuarioPwd: password,
      nIntentos: '',
    }
  )

  if (tokenAutentificacion) {
    const login = await axios.post(
      `${API_SERVICE_WRAPPER}/Login
      `,
      {
        Usuario: username,
        Password: password,
        NumIntentosLogin: 0,
        HostGuid: '',
      }
    )

    if (login && login.data.Usuario.includes(PORTAL_ROLE)) {
      const obtenerPerfil = await axios.get(
        `${API_SERVICE_WRAPPER}/ObtenerPerfil`,
        { login: username }
      )

      if (obtenerPerfil && !obtenerPerfil.data.Error) {
        res.status(200).send({
          token: tokenAutentificacion.data.token,
          username: obtenerPerfil.data.Objeto.NombreUsuario,
          email: obtenerPerfil.data.Objeto.Mail,
        })
      } else {
        res.status(401).send({
          message: 'Error al obtener el email del usuario',
        })
      }
    } else {
      res.status(401).send({
        message: 'Usuario o contraseña incorrectos',
      })
    }
  } else {
    res.status(401).send({
      message: 'Usuario o contraseña incorrectos',
    })
  }
})

app.get('/jwt/logout', (req, res) => {
  res.clearCookie('token')
  res.clearCookie('username')
  res.clearCookie('email')
  res.redirect(302, '/')
})

app.post('/api/mailto', (req, res) => {
  res.status(200).send()
})

app.get('/api/permission', (req, res) => {
  res.status(200).send({
    menu: [
      {
        key: '/dashboard',
        value: 'Dashboard',
      },
      {
        key: '/amis',
        value: 'Movimientos AMIS',
      },
    ],
    url: ['/login', '/dashboard', '/amis', '/amis/detail', '/search'],
  })
})

app.get('/api/dashboard/graph', (req, res) => {
  res.status(200).send({
    total: 6078000,
    individual: 3000,
    flotilla: 3000,
    graph: {
      individualResidente: 1693400,
      individualTurista: 1600,
      flotillaResidente: 3000,
    },
  })
})

app.get('/api/dashboard/graph/history', (req, res) => {
  res.status(200).send({
    total: 6000,
    totalesEstatus: {
      porcentajeError: 0.05,
      extraidos: 6000,
      procesados: 4000,
      exitosos: 3800,
      reintentos: 15,
      error: 200,
    },
    months: [
      {
        monthYear: '202005',
        individualResidente: 9500,
        individualTuristas: 1500,
        flotillaResidente: 2950,
        error: 50,
      },
      {
        monthYear: '202004',
        individualResidente: 9500,
        individualTuristas: 20001500,
        flotillaResidente: 2950,
        error: 50,
      },
      {
        monthYear: '202003',
        individualResidente: 1500,
        individualTuristas: 1500,
        flotillaResidente: 2950,
        error: 50,
      },
      {
        monthYear: '202002',
        individualResidente: 1500,
        individualTuristas: 1500,
        flotillaResidente: 2950,
        error: 50,
      },
    ],
  })
})

app.get('/api/dashboard/graph/processingHistory', (req, res) => {
  res.status(200).send({
    historia: [
      {
        fechaHora: '2018-07-14 17:45:55.948',
        estatus: 'ok',
        descripcion: 'AltaNocturna',
        registros: 6000,
      },
      {
        fechaHora: '2018-07-14 17:45:55.948',
        estatus: 'ok',
        descripcion: 'AltaNocturna',
        registros: 6000,
      },
      {
        fechaHora: '2018-07-14 17:45:55.948',
        estatus: 'ok',
        descripcion: 'AltaNocturna',
        registros: 6000,
      },
      {
        fechaHora: '2018-07-14 17:45:55.948',
        estatus: 'ok',
        descripcion: 'AltaNocturna',
        registros: 6000,
      },
      {
        fechaHora: '2018-07-14 17:45:55.948',
        estatus: 'ok',
        descripcion: 'AltaNocturna',
        registros: 6000,
      },
    ],
  })
})

app.post('/api/amis/search', (req, res) => {
  res.status(200).send({
    total: 12214,
    data: [
      {
        policy: '211-8100052187',
        agent: '58412',
        insureType: '4014',
        vin: 'KL1JJ51Z57K693777',
        bussinessName: 'DANIEL CASTILLO GUERRERO',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10620,
        vehiculoId: 12532,
      },
      {
        policy: '58-122166',
        agent: '53633',
        insureType: '4014',
        vin: '4T4BE46K39R105472',
        bussinessName: 'ROBERTO CASTANARES IBARRA',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10616,
        vehiculoId: 12527,
      },
      {
        policy: '295-6860',
        agent: '63200',
        insureType: '4013',
        vin: 'MA6CC6CD3JT009345',
        bussinessName: 'FRANCISCO GONZALEZ MARTINEZ',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10617,
        vehiculoId: 12529,
      },
      {
        policy: '211-8200165352',
        agent: '58412',
        insureType: '4013',
        vin: '3G1SF2421XS136034',
        bussinessName: 'LUZ MARIA HERNANDEZ FLORES',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10618,
        vehiculoId: 12530,
      },
      {
        policy: '535-558',
        agent: '90111',
        insureType: '4013',
        vin: '3GA0C122XKM003816',
        bussinessName: 'JOSE LUIS GONZALEZ ALVAREZ',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10619,
        vehiculoId: 12531,
      },
      {
        policy: '211-8100165426',
        agent: '58412',
        insureType: '4013',
        vin: '5KBRL38675B800846',
        bussinessName: 'MA GUADALUPE CASTANON ANTOPIA',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10611,
        vehiculoId: 12522,
      },
      {
        policy: '3-366127',
        agent: '1134',
        insureType: '4013',
        vin: 'JM1BN1L35J1193344',
        bussinessName: 'MARCO AURELIO GARCIA DE ANDA',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10612,
        vehiculoId: 12523,
      },
      {
        policy: '8-172170',
        agent: '90203',
        insureType: '4013',
        vin: '3N1BC1AS9DK228098',
        bussinessName: 'NADIA ANALI LOPEZ ACOSTA',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10613,
        vehiculoId: 12524,
      },
      {
        policy: '8-172170',
        agent: '90203',
        insureType: '4013',
        vin: '3N1BC1AS9DK228098',
        bussinessName: 'NADIA ANALI LOPEZ ACOSTA',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10613,
        vehiculoId: 12528,
      },
      {
        policy: '24-116320',
        agent: '52465',
        insureType: '4013',
        vin: '9BFBT11N778002135',
        bussinessName: 'LORENZO ANTONIO CHALE PECH',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10614,
        vehiculoId: 12525,
      },
      {
        policy: '68-66425',
        agent: '53789',
        insureType: '4013',
        vin: '466601',
        bussinessName: 'JAVIER CEBALLOS NAVA',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10615,
        vehiculoId: 12526,
      },
      {
        policy: '5-386384',
        agent: '10291',
        insureType: '4013',
        vin: 'VSSEM46L79R033815',
        bussinessName: 'MIGUEL IGNACIO AMEZQUITA DIAZ',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10609,
        vehiculoId: 12520,
      },
      {
        policy: '211-8100165403',
        agent: '58412',
        insureType: '4013',
        vin: '3N1CK3CD0FL255964',
        bussinessName: 'ENEDINA GUADALUPE GALLARDO VALDEZ',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10610,
        vehiculoId: 12521,
      },
      {
        policy: '233-2273',
        agent: '66656',
        insureType: '4013',
        vin: 'WF0LP3XH6A1120996',
        bussinessName: 'HOMERO BELTRAN SALINAS',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10607,
        vehiculoId: 12518,
      },
      {
        policy: '8-164753',
        agent: '52563',
        insureType: '4013',
        vin: '1J4RH4GK2AC157473',
        bussinessName: 'LUIS FERNANDO BOTELLO COSTILLA',
        date: '20200913',
        status: 'En proceso',
        movimientoId: 10608,
        vehiculoId: 12519,
      },
    ],
  })
})

app.get('/api/amis/excel', (req, res) => {
  res.download('src/files/Book1.xlsx', 'excel.xlsx')
})

app.get('/api/amis/movimientos/:movimientosId', (req, res) => {
  res.status(200).send({
    id: req.params.movimientosId,
    status: 'process',
    date: '20200511',
    policyType: 4214,
    insuranceData: {
      typeInsurance: 4,
      policyNumber: 'DVNIOFSVSFVSF',
      issueDate: '20201007',
      movementType: 1,
      startValidity: '20201007',
      finalValidity: '20201007',
    },
    client: {
      lastName: 'Maceda',
      secondLastName: 'Bouchan',
      name: 'Sinuhe',
      curp: 'DFDCEGR7481',
      rfc: 'DFDCEGR7481',
    },
    beneficiary: {
      lastName: 'Maceda',
      secondLastName: 'Bouchan',
      name: 'Sinuhe',
      curp: 'DFDCEGR7481',
      rfc: 'DFDCEGR7481',
    },
    salesChannel: {
      salesChannel: 2,
      agency: 3214,
      agent: 90399,
      locationInsured: 544,
      agentLocation: 544,
    },
    coverage: [
      {
        id: 1,
        insuranceSumId: 1,
        insuranceSum: 55555,
        status: 01,
      },
      {
        id: 1,
        insuranceSumId: 1,
        insuranceSum: 55555,
        status: 01,
      },
      {
        id: 1,
        insuranceSumId: 1,
        insuranceSum: 55555,
        status: 01,
      },
    ],
  })
})

app.get('/api/amis/movimientos/vehicles/:movimientosId', (req, res) => {
  res.status(200).send({
    total: 2,
    data: [
      {
        vin: 'MVB2EDFDFDRSFS',
        use: 1,
        required: 0,
        model: 2001,
        licensePlate: '20GFH',
        inciso: 16,
        brand: 2489,
        client2: 'Ricardo Esparza',
        Beneficiary: 'Ricardo Esparza',
      },
      {
        vin: 'MVB2EDF23FRSFS',
        use: 1,
        required: 0,
        model: 2001,
        licensePlate: '21GFH',
        inciso: 16,
        brand: 2489,
        client2: 'Ricardo Esparza',
        beneficiary: 'Ricardo Esparza',
      },
    ],
  })
})

app.get('/api/amis/movimientos/flotilla/detalles', (req, res) => {
  res.status(200).send({
    client2: {
      ApellidoMaterno: 'Perez',
      ApellidoPaterno: 'Jimenez',
      nombre: 'Jose',
      curp: 'JJPP901212LJSDD3PO',
      rfc: 'JJPP901212',
    },
    beneficiary: {
      lastName: 'Maceda',
      secondLastName: 'Bouchan',
      name: 'Sinuhe',
      curp: 'DFDCEGR7481',
      rfc: 'DFDCEGR7481',
    },
    coverage: [
      {
        id: 1,
        insuranceSumId: 1,
        insuranceSum: 55555,
        status: 1,
      },
      {
        id: 2,
        insuranceSumId: 444,
        insuranceSum: 55554445,
        status: 1,
      },
      {
        id: 3,
        insuranceSumId: 1,
        insuranceSum: 554541555,
        status: 1,
      },
    ],
  })
})

app.get('/api/search/:policyId', (req, res) => {
  res.status(200).send({
    policyId: req.params.policyId,
    status: 'cancel',
    vin: '1GNCS4564165M5',
    date: '20170101',
    insuranceCompany: [
      {
        insuranceTypeId: 1,
        policy: '00056878952',
        dateOfIssue: '20170101',
        moveType: 2,
        initValidity: '20180101',
        endValidity: '20300101',
      },
      {
        insuranceTypeId: 2,
        policy: '00056878952',
        dateOfIssue: '20170101',
        moveType: 2,
        initValidity: '20180101',
        endValidity: '20300101',
      },
      {
        insuranceTypeId: 3,
        policy: '00056878952',
        dateOfIssue: '20170101',
        moveType: 2,
        initValidity: '20180101',
        endValidity: '20300101',
      },
      {
        insuranceTypeId: 4,
        policy: '00056878952',
        dateOfIssue: '20170101',
        moveType: 2,
        initValidity: '20180101',
        endValidity: '20300101',
      },
    ],
    coverage: [
      {
        id: 1,
        insuranceSumId: 1,
        insuranceSum: 55555,
        status: 01,
      },
      {
        id: 1,
        insuranceSumId: 1,
        insuranceSum: 55555,
        status: 01,
      },
      {
        id: 1,
        insuranceSumId: 1,
        insuranceSum: 55555,
        status: 01,
      },
    ],
  })
})

app.get('/api/search/policy/vehicles/:policyId', (req, res) => {
  res.status(200).send({
    total: 2,
    data: [
      {
        vin: 'MVB2EDFDFDRSFS',
        use: 1,
        required: 0,
        model: 2001,
        licensePlate: '20GFH',
        inciso: 16,
        brand: 2489,
        client2: 'Ricardo Esparza',
        Beneficiary: 'Ricardo Esparza',
      },
      {
        vin: 'MVB2EDF23FRSFS',
        use: 1,
        required: 0,
        model: 2001,
        licensePlate: '21GFH',
        inciso: 16,
        brand: 2489,
        client2: 'Ricardo Esparza',
        beneficiary: 'Ricardo Esparza',
      },
    ],
  })
})

app.get('/api/search/excel/:policyId', (req, res) => {
  res.download('src/files/Book1.xlsx', 'excel.xlsx')
})

app.post('/api/search/mailto/:policyId', (req, res) => {
  res.status(200).send()
})

/**
 * MOCK URL
 */

// http://servicewrapper.ts07.hdi.net:8000/api/Security/TokenAutentificacion
app.post('/TokenAutentificacion', (req, res) => {
  res.status(200).send({
    identityId: 'us-east-1:d33edcc8-0aa4-491e-b3ec-57b72403b6da',
    token: 'eyJraWQiOiJ1cy1QtMTozZTcxYWI5OS0wNDdhLTQ5N=',
    mensaje: null,
  })
})

// http://servicewrapper.ts07.hdi.net:8000/api/Security/ObtenerPerfil?login=001892
app.get('/ObtenerPerfil', (req, res) => {
  res.status(200).send({
    Error: false,
    Mensaje: '',
    Objeto: {
      NombreUsuario: 'Urbano Rodriguez',
      Mail: 'urbanorm@hotmail.com',
      Agencia: '00003',
      TipoUsuario: 5051,
    },
  })
})

// http://servicewrapper.ts07.hdi.net:8000/api/Security/Login
app.post('/Login', (req, res) => {
  // res
  //   .status(200)
  //   .send(
  //     '{"mensaje":"","redireccionar":"/Paginas/Default/Inicio.aspx?CodigoReferente=Login","error":false,"mensajeError":"","Usuario":["Agente","DAN - AGENTE","PROP AGENTE A","SO - Estado_de_cuenta_Agente_Clave_L"],"Agencia":"00003"}'
  //   )

  res.status(200).send('DAN - AGENTE')
})

/**
 * END MOCK
 */

/*
app.get('', (req, res) => {
  res.status(200).send()
})
*/

app.listen(PORT, () => {
  console.log(
    'mock-services listening on port http://localhost:' + PORT + '/api'
  )
})
