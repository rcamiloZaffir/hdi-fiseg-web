import {
  SET_LOGIN,
  GET_LOGIN,
  REQ_LOGIN,
  IS_LOGGED,
} from '@actions/actionTypes'

const INITIAL_STATE = {
  login: {},
  user: {},
  reqLogin: false,
}

export default (state = INITIAL_STATE, action = null) => {
  switch (action.type) {
    case SET_LOGIN:
      return {
        ...state,
        login: action.payload,
      }

    case GET_LOGIN:
      return { ...state, user: action.payload }
    case REQ_LOGIN:
      return { ...state, reqLogin: true }
    case IS_LOGGED:
      return { ...state, reqLogin: false }

    default:
      return state
  }
}
