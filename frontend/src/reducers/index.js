import { combineReducers } from 'redux'
import errorHandler from './errorHandler'
import spinner from './spinner'
import login from './login'
import modal from './modal'
import permission from './permission'
import http from './http'
import form from './form'

export default combineReducers({
  errorHandler,
  login,
  spinner,
  modal,
  permission,
  http,
  form,
})
