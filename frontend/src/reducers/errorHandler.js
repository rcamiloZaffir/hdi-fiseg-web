/**
 * Reducer para manejo de errores
 * @module reducers/errorHandler
 */
import { MESSAGE, NOTIFICATION, MODAL } from '@utils/constants'
import {
  CLOSE_HANDLER,
  IS_LOADING,
  LAUNCH_MESSAGE,
  LAUNCH_MODAL,
  LAUNCH_NOTIFY,
} from '@actions/actionTypes'

const INITIAL_STATE = {
  statusCode: null,
  message: '',
  title: '',
  type: '',
  level: '',
  open: false,
  loading: true,
}

/**
 * Swithc de reducer con estados
 */
export default (state = INITIAL_STATE, action = null) => {
  switch (action.type) {
    case LAUNCH_MESSAGE: {
      return {
        ...state,
        type: MESSAGE,
        open: true,
        loading: false,
        ...action.payload,
      }
    }
    case LAUNCH_MODAL: {
      return {
        ...state,
        type: MODAL,
        open: true,
        loading: false,
        route: action.route,
        ...action.payload,
      }
    }
    case LAUNCH_NOTIFY: {
      return {
        ...state,
        type: NOTIFICATION,
        open: true,
        loading: false,
        route: action.route,
        ...action.payload,
      }
    }
    case CLOSE_HANDLER:
      return {
        ...state,
        open: false,
        statusCode: null,
        message: '',
        title: '',
        route: action.route,
        type: '',
        level: '',
        loading: false,
      }
    case IS_LOADING:
      return { ...state, loading: true }

    default:
      return state
  }
}
