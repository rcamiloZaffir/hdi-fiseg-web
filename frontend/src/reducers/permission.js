/**
 * Reucer para maejo de permisos
 * @module reducers/permission
 */
import { GET_PERMISSION } from '@actions/actionTypes'

const INITIAL_STATE = {
  menu: [],
  url: [],
}

export default (state = INITIAL_STATE, action = null) => {
  switch (action.type) {
    case GET_PERMISSION:
      return {
        ...state,
        menu: action.payload.menu,
        url: action.payload.url,
      }

    case null:
      return null

    default:
      return state
  }
}
