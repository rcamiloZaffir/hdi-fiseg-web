/**
 * Reducer para manejo de formularios
 * @module reducers/form
 */
import {
  FORM_INIT,
  FORM_CHANGE,
  FORM_VALIDATE,
  FORM_RESET,
  FORM_UPDATE,
  FORM_PUT,
} from '@actions/actionTypes'

const INITIAL_STATE = {
  validate: {},
  hasFeedback: false,
  form: {},
}

export default (state = INITIAL_STATE, action = null) => {
  const { type, payload } = action

  switch (type) {
    case FORM_INIT: {
      return {
        ...INITIAL_STATE,
        form: payload.form,
      }
    }

    case FORM_UPDATE: {
      return {
        ...state,
        validate: {},
        hasFeedback: false,
        form: { ...state.form, ...payload.form },
      }
    }

    case FORM_CHANGE: {
      const { name, value } = payload
      return {
        ...state,
        form: {
          ...state.form,
          [name]: value,
        },
      }
    }
    case FORM_VALIDATE: {
      return {
        ...state,
        validate: payload.validate,
        hasFeedback: true,
      }
    }
    case FORM_RESET: {
      return {
        ...state,
        validate: payload.validate,
        hasFeedback: payload.hasFeedback,
        form: payload.form,
      }
    }

    case FORM_PUT: {
      const { name, value } = payload
      return {
        ...state,
        [name]: value,
      }
    }

    default:
      return state
  }
}
