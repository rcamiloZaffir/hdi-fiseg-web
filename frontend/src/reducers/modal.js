/**
 * Reducer para manejo de estados del modal
 * @module reducers/errorHandler
 */
import { OPEN_MODAL, CLOSE_MODAL } from '@actions/actionTypes'

const INITIAL_STATE = {
  open: false,
}

export default (state = INITIAL_STATE, action = null) => {
  switch (action.type) {
    case OPEN_MODAL:
      return { ...state, open: true }
    case CLOSE_MODAL:
      return { ...state, open: false }
    default:
      return state
  }
}
