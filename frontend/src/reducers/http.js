/**
 * Reducer para manejo de peticiones htto
 * @module reducers/http
 */
import { HTTP_GET, HTTP_POST, HTTP_INIT } from '@actions/actionTypes'

const INITIAL_STATE = {}

export default (state = INITIAL_STATE, action = null) => {
  const { type, payload } = action

  switch (type) {
    case HTTP_GET:
    case HTTP_POST:
      return {
        ...state,
        [payload.path]: payload.data,
      }

    case HTTP_INIT:
      return INITIAL_STATE

    default:
      return state
  }
}
