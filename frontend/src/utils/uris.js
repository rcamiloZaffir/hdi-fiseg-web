import loadable from '@loadable/component'

const getPage = page => loadable(() => import(`../pages/${page}`), {})

const uris = {
  login: {
    index: { uri: '/login', component: getPage('Login'), private: false },
  },

  dashboard: {
    index: {
      uri: '/dashboard',
      component: getPage('Dashboard'),
      private: true,
    },
  },

  amis: {
    index: {
      uri: '/amis',
      component: getPage('Amis'),
      private: true,
    },
  },

  amisDeatil: {
    index: {
      uri: '/amis/detail',
      component: getPage('AmisDetail'),
      private: true,
    },
  },

  search: {
    index: {
      uri: '/search',
      component: getPage('Search'),
      private: true,
    },
  },
}

export default uris
