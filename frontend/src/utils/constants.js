export const FADE_TRANSITION = { opacity: 1, from: { opacity: 0 } }

// Color
export const COLOR = {
  primary: '#006729',
  secondary: '#79b530',
  info: '#036be4',
  success: '#79b530',
  warning: '#faad14',
  error: '#e60018',
  processing: '#036be4',
  gray: '#6c6f70',
}

export const TABLE_PAGE_CURREN_PAGE_ZISE = 15

// HDI Status
export const HDI_STATUS = {
  extract: 'extract',
  process: 'process',
  success: 'success',
  error: 'error',
  refresh: 'refresh',
  all: 'all',
}

// policyType TIPO DE CARRO
export const INSURE_TYPE_NAME = {
  1: 'Individual',
  2: 'Flotilla',
  3: 'Turista',
}

export const INSURE_TYPE = {
  INDIVIDUAL: 1,
  FLOTILLA: 2,
  TURISTA: 3,
}

export const POLICY_STATUS = {
  cancel: 'CANCELADO',
  duplicate: 'VIN DUPLICADO',
}

// HDI Filters
export const HDI_FILTERS = [
  { value: 'Datos extraidos', text: 'Datos extraídos', id: 1 },
  { value: 'En proceso', text: 'En proceso', id: 2 },
  { value: 'Enviado', text: 'Enviado', id: 3 },
  { value: 'Exito', text: 'Éxito', id: 4 },
  { value: 'Error de extracción', text: 'Error de extracción', id: 5 },
  { value: 'Error de transformación', text: 'Error de transformación', id: 6 },
  { value: 'Error de envío', text: 'Error de envío', id: 7 },
]

// OrderStatus
export const ORDER_STATUS = {
  policy: 'NumeroPoliza',
  agent: 'AgenteId',
  insureType: 'TipoPoliza',
  vin: 'Vin',
  bussinessName: 'Nombre',
  date: 'FechaMovimiento',
  status: 'EstatusProcesamiento',
}

// DATE
export const DATE_DDMMYYYY = 'DD/MM/YYYY'
export const DATE_DDMMYYYY2 = 'DD-MM-YYYY'
export const DATE_YYYYMMDD = 'YYYYMMDD'
export const DATE_DDMMYYYY_HHMM = 'DD/MM/YYYY hh:mm:ss A'

// Http methods
export const GET = 'GET'
export const POST = 'POST'
export const PUT = 'PUT'
export const DELETE = 'DELETE'
export const PATCH = 'PATCH'

// MessageTypes
export const MESSAGE = 'message'
export const NOTIFICATION = 'notification'
export const MODAL = 'modal'

// Message Levels
export const INFO = 'info'
export const SUCCESS = 'success'
export const ERROR = 'error'
export const WARNING = 'warning'

export const pdfOptions = {
  margin: 0.3,
  image: {
    type: 'png',
    quality: 1,
  },
  html2canvas: {
    scale: 2,
  },
  jsPDF: {
    unit: 'in',
    format: 'a4',
    orientation: 'landscape',
  },
}

// messagesValidations
export const MESSAGE_VALIDATIONS = {
  'string.base': `Solo estan permitido los valores alfabéticos`,
  'string.empty': `El campo no puede estar vacío`,
  'string.base64.required': `No se ha agregado ningún archivo`,
  'string.min': `La logitud mínima del campo es   {#limit}`,
  'string.max': `La longitud máxima del campo es  {#limit}`,
  'string.required': `Campo requerido`,
  'string.alphanum': `Solo estan permitido los valores alfanuméricos`,
  'string.email':
    'El formato del correo es inválido, debe contener "@" & ".com',
  'any.only': 'El campo debe ser igual al password',
  'any.required': 'Seleccione una opción',
  'array.min': 'Debe seleccionar al menos una opción',
  'number.base': `Solo estan permitido los valores numéricos`,
  'number.min': `El valór mínimo es  {#limit}`,
  'number.max': `El valór máximo es  {#limit}`,
  'number.integer': 'No se aceptan decimales',
  'number.positive': 'El número ingresado debe ser mayor a 0',
}

export const STATUS_CODE_MESSAGES = {
  200: {
    level: 'success',
    title: 'Success',
    message: 'Ok',
  },
  201: {
    level: 'success',
    title: 'Created',
    message: 'Ok',
  },
  202: {
    level: 'success',
    title: 'Accepted',
    message: 'Ok',
  },
  204: {
    level: 'success',
    title: 'No Content',
  },
  400: {
    level: 'error',
    title: 'Bad Request',
  },
  401: {
    level: 'error',
    title: 'Unauthorized',
  },
  402: {
    level: 'error',
    title: 'Payment Required',
  },
  403: {
    level: 'error',
    title: 'Forbidden',
  },
  404: {
    level: 'error',
    title: 'NotFound',
  },
  405: {
    level: 'error',
    title: 'Method Not Allowed',
  },
  406: {
    level: 'error',
    title: 'Not Acceptable',
  },
  407: {
    level: 'error',
    title: 'Proxy Authentication Required',
  },
  408: {
    level: 'error',
    title: 'Request Timeout',
  },
  409: {
    level: 'error',
    title: 'Conflict',
  },
  410: {
    level: 'error',
    title: 'Gone',
  },
  411: {
    level: 'error',
    title: 'Length Required',
  },
  412: {
    level: 'error',
    title: 'Precondition Failed',
  },
  413: {
    level: 'error',
    title: 'Payload TooLarge',
  },
  414: {
    level: 'error',
    title: 'URI TooLong',
  },
  415: {
    level: 'error',
    title: 'Unsupported Media Type',
  },
  500: {
    level: 'error',
    title: 'Internal Server Error',
    message: 'The request Server Is Not Available',
  },
  501: {
    level: 'error',
    title: 'Not Implemented',
  },
  502: {
    level: 'error',
    title: 'Bad Gateway',
  },
  503: {
    level: 'error',
    title: 'Service Unavailable',
  },
  504: {
    level: 'error',
    title: 'Gateway Timeout',
  },
  505: {
    level: 'error',
    title: 'HTTP Version Not Supported',
  },
  506: {
    level: 'error',
    title: 'Variant Also Negotiates',
  },
  507: {
    level: 'error',
    title: 'Insufficient Storage',
  },
  508: {
    level: 'error',
    title: 'Loop Detected',
  },
  509: {
    level: 'error',
    title: 'Bandwidth Limit Exceeded',
  },
  510: {
    level: 'error',
    title: 'Not Extended',
  },
  511: {
    level: 'error',
    title: 'Network Authentication Required',
  },
}
