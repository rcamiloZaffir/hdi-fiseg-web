/**
 * Module utils para usar funciones genericas en todo el proyecto
 * @module utils
 */
import numbro from 'numbro'
import millify from 'millify'
import Cookies from 'js-cookie'
import moment from 'moment'
import { DATE_DDMMYYYY, DATE_YYYYMMDD, DATE_DDMMYYYY_HHMM } from './constants'

/**
 * Function para retornas paramaetros de url por get
 * @param {String} name nombre a mapear paramas
 */
export function urlParams(name) {
  return new URLSearchParams(window.location.search).get(name)
}

/**
 * Capitalizar texto
 * @param {String} str Texto
 */
export function capitalize(str) {
  if (typeof str !== 'string') return ''
  return str.charAt(0).toUpperCase() + str.slice(1)
}

/**
 * Formato por comas de separacion
 * @param {Integer} val numero entero
 */
export const number = val => numbro(val).format({ thousandSeparated: true })

/**
 * Formato en comas de separacion
 * @param {Integer} val numeroentero
 */
export const number2 = val => {
  const num = numbro(val)

  if (val === 0) {
    return 0
  }
  if (val < 999) {
    return num.format({
      thousandSeparated: true,
    })
  }

  return millify(val, {
    units: ['', 'k', 'M', 'G', 'T', 'P', 'E'],
    space: true,
  })
}

/**
 * Formato DDMMYYY
 * @param {Date} date Fecha en valor Date
 */
export const dateformat = date => moment(date).format(DATE_DDMMYYYY)

/**
 * Formato YYYYMMDD
 * @param {Date} date Fecha en valor Date
 */
export const dateformat2 = date => moment(date).format(DATE_YYYYMMDD)

/**
 * Formato DDMMYYY
 * @param {Date} date Fecha en valor Date
 */
export const dateformatHour = date => moment(date).format(DATE_DDMMYYYY_HHMM)

/**
 * isAuthenticated
 */
export const isAuthenticated = () => {
  const token = Cookies.get('token')

  if (token) {
    return {
      username: Cookies.get('username'),
      email: Cookies.get('email'),
    }
  }
  return false
}

/**
 * JoyErros
 */
export const parseJoyErrors = array => {
  const errors = {}
  for (let i = 0; i < array.length; i++) {
    const item = array[i]
    errors[item.context.key] = {
      message: item.message,
      type: item.type,
    }
  }
  return errors
}

/**
 * Funcion para descargar archivos por http post
 * @param {blob} blob Archivo en formato blob
 * @param {String} name nombre del archivo a descargar
 */
export function downloadBlob(blob, name) {
  // Convert your blob into a Blob URL (a special url that points to an object in the browser's memory)
  const blobUrl = URL.createObjectURL(blob)

  // Create a link element
  const link = document.createElement('a')

  // Set link's href to point to the Blob URL
  link.href = blobUrl
  link.download = name

  // Append link to the body
  document.body.appendChild(link)

  // Dispatch click event on the link
  // This is necessary as link.click() does not work on the latest firefox
  link.dispatchEvent(
    new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window,
    })
  )

  // Remove link from body
  document.body.removeChild(link)
}
