/**
 * Module de funciones genericas de http
 * @module utils/httpService
 */
/* eslint-disable prefer-promise-reject-errors */
import axios from 'axios'
import {
  GET,
  POST,
  PUT,
  DELETE,
  PATCH,
  STATUS_CODE_MESSAGES,
} from '@utils/constants'
import { v4 as uuidv4 } from 'uuid'
import moment from 'moment'
import querystring from 'querystring'

export const protocol = process.env.REACT_APP_HTTP_REQUEST_PROTOCOL
export const host = process.env.REACT_APP_HTTP_REQUEST_HOST
export const port = process.env.REACT_APP_HTTP_REQUEST_PORT

const defaultObject = {
  level: 'success',
  message: 'A module has been changed',
  title: 'Success',
}

const setMessage = ({ response, onSuccess, level }) => {
  const statusCodeMessageObject =
    STATUS_CODE_MESSAGES[response.status] || defaultObject
  return {
    ...statusCodeMessageObject,
    level: level || statusCodeMessageObject.level,
    message: onSuccess || statusCodeMessageObject.message,
  }
}

export const buildUrlBase = (path, params, method) => {
  const querys = method === GET ? querystring.stringify(params) : ''
  // const base = `${protocol}://${host}:${port}`
  // return `${base}${path}?${querys}`

  if (['/jwt/login', '/jwt/logout'].includes(path)) {
    return `${path}?${querys}`
  }

  return `/api${path}?${querys}`
}

/**
 * @description SET @axios CONFIG, SEND HTTP REQUEST, WRAP RESPONSE
 * @param {Object} options
 * @example
 * {
 *  baseUrl: [if have an external baseurl]
 *  method: [POST, GET, PUT, DELETE]
 *  path: /foo/bar/id
 *  data: {}
 *  headers: {}
 *  message: {
 *   level: [error, info, success, warning],
 *   onSuccess: A success message
 *  }
 * }
 */
export const ClientHttpRequest = async options => {
  const { message, params, method, path } = options

  /**
   * BUILD URL WITH ENVIRONMENTS AND SETTINGS WITH EXTERNAL URLBASE
   */
  const url = buildUrlBase(path, params, method)
  const data = params

  /**
   * BUILD AXIOS OPTIONS
   */
  const CurrentDate = moment().format()
  const key = uuidv4()

  const settings = {
    withCredentials: true,
    headers: {
      ...options.headers,
      Accept: 'application/json',
      Locale: 'es-MX',
      request_date_time: CurrentDate,
      'Content-Type': 'application/json',
      transaction_id: key,
    },
  }
  try {
    /**
     * SWITCH REQUEST METHOD
     * AXIOS METHODS MUST BE SEPARATED
     */
    switch (method) {
      case GET: {
        // AWAIT TO RESPONSE
        const response = await axios.get(url, { ...settings })
        const Message = setMessage({ ...message, response })
        return { response, Message }
      }
      case POST: {
        // AWAIT TO RESPONSE
        const response = await axios.post(url, data, { ...settings })
        const Message = setMessage({ ...message, response })
        return { response, Message }
      }
      case PUT: {
        // AWAIT TO RESPONSE
        const response = await axios.put(url, data, { ...settings })
        const Message = setMessage({ ...message, response })
        return { response, Message }
      }
      case DELETE: {
        // AWAIT TO RESPONSE
        const response = await axios.delete(url, data, { ...settings })
        const Message = setMessage({ ...message, response })
        return { response, Message }
      }
      case PATCH: {
        // AWAIT TO RESPONSE
        const response = await axios.patch(url, data, { ...settings })
        const Message = setMessage({ ...message, response })
        return { response, Message }
      }
      default:
        return {}
    }
  } catch (error) {
    let Message = {}
    if (error.response) {
      Message = setMessage({
        ...message,
        response: error.response,
      })
      return { error, Message }
    }
    Message = setMessage({
      response: {
        status: 500,
      },
    })
    return Promise.reject({ error, Message }) // Rejects object prefer new Error
  }
}

export default null
