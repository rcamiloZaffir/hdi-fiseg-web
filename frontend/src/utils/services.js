export const LOGIN = '/jwt/login'
export const LOGOUT = '/jwt/logout'
export const PERMISSION = '/permission'

export const MAIL_TO = '/mailto'

export const DASHBOARD_GRAPH = '/dashboard/graph'
export const DASHBOARD_GRAPH_HISTORY = '/dashboard/graph/history'
export const DASHBOARD_GRAPH_PROCESSING_HISTORY =
  '/dashboard/graph/processingHistory'

export const AMIS_SEARCH = '/amis/search'
export const AMIS_EXCEL = '/amis/excel'
export const AMIS_MOVIMIENTOS = '/amis/movimientos'
export const AMIS_MOVIMIENTOS_VEHICLES = '/amis/movimientos/vehicles'
export const AMIS_MOVIMIENTOS_FLOTILLA_DETALLE =
  '/amis/movimientos/flotilla/detalles'

export const SEARCH = '/search'
export const SEARCH_POLICY_VEHICLES = '/search/policy/vehicles'

export const SEARCH_EXCEL = '/search/excel'
export const SEARCH_MAIL_TO = '/search/mailto'
