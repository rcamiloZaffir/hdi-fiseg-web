/**
 * Actions de Manejo de eerores
 * @module actions/errorHandler
 */
import {
  CLOSE_HANDLER,
  LAUNCH_MESSAGE,
  LAUNCH_MODAL,
  LAUNCH_NOTIFY,
} from './actionTypes'

/**
 *
 * @param {*} route Path that comes from modal or notify
 */
export const closeNotification = route => {
  if (!route) {
    return { type: CLOSE_HANDLER }
  }
  return {
    type: CLOSE_HANDLER,
    // redirect: hashHistory.push(route),
  }
}
/**
 * New Messages
 */
export const launchMessage = payload => ({
  type: LAUNCH_MESSAGE,
  payload,
})

/**
 *
 * @param {*} payload Message config from backend
 * @param {*} route Path to be redirected when closing notification
 */
export const launchModal = (payload, route = null) => ({
  type: LAUNCH_MODAL,
  payload,
  route,
})

/**
 *
 * @param {*} payload Message config from backend
 * @param {*} route Path to be redirected when closing notification
 */
export const launchNotify = (payload, route = null) => {
  return {
    type: LAUNCH_NOTIFY,
    payload,
    route,
  }
}
