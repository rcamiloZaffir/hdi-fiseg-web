/**
 * Actions de spinner
 * @module actions/spinner
 */
import { IS_LOADING, IS_LOADED } from './actionTypes'

/**
 * Action para loading
 */
export const loading = () => ({
  type: IS_LOADING,
})

/**
 * Action para mostrar que esta en estado loaded
 */
export const loaded = () => ({
  type: IS_LOADED,
})
