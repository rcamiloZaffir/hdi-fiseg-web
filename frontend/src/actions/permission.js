import { GET_PERMISSION } from './actionTypes'

export const getPermission = () => ({
  type: GET_PERMISSION,
  payload: {
    menu: [
      {
        key: '/dashboard',
        value: 'Dashboard',
      },
      {
        key: '/amis',
        value: 'Movimientos AMIS',
      },
    ],
    url: ['/login', '/dashboard', '/amis', '/amis/detail', '/search'],
  },
})
