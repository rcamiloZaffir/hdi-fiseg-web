/**
 * Actions de peticiones http
 * @module actions/http
 */
import { MODAL, WARNING, GET, POST } from '@utils/constants'
import { ClientHttpRequest } from '@utils/httpService'
import { launchModal, launchMessage } from './errorHandler'
import { loaded, loading } from './spinner'
import { HTTP_GET, HTTP_POST, HTTP_INIT } from './actionTypes'

/**
 * Servicio http por metodo GET
 * @param {String} path nombre del path del servicio
 * @param {JSON} form JSon del formulario
 * @param {Function} callback Funcion de callback
 */
const httpGet = (path, form = {}, callback = () => {}) => async dispatch => {
  dispatch(loading())

  try {
    const data = await ClientHttpRequest({
      method: GET,
      path,
      params: form,
    })

    dispatch(loaded())
    if (data.response) {
      const myData = data.response.data
      dispatch({
        type: HTTP_GET,
        payload: {
          path,
          data: myData,
        },
      })
      callback(myData)
    } else if (data.error) {
      const { message } = data.error.response.data
      const messageGetError = {
        type: MODAL,
        level: WARNING,
        message: message ? message : 'Error de conexión con el servidor',
      }

      dispatch(launchModal(messageGetError))
    }
  } catch (error) {
    const myError = error.Message
    dispatch(loaded())
    dispatch(launchMessage(myError))
  }
}

/**
 * Servicio http por metodo GET
 * @param {String} path nombre del servicio
 * @param {*} form JSon del formulario
 * @param {*} callback Function de callback
 */
const httpPost = (path, form = {}, callback = () => {}) => async dispatch => {
  dispatch(loading())

  try {
    const data = await ClientHttpRequest({
      method: POST,
      path,
      params: form,
    })

    dispatch(loaded())
    if (data.response) {
      dispatch({
        type: HTTP_POST,
        payload: {
          path,
          data: data.response.data,
        },
      })
      callback(data.response.data)
    } else if (data.error) {
      const { message } = data.error.response.data
      const messageError = {
        type: MODAL,
        level: WARNING,
        message: message ? message : 'Error de conexión con el servidor',
      }

      dispatch(launchModal(messageError))
    }
  } catch (error) {
    dispatch(loaded())
    dispatch(launchMessage(error.Message))
  }
}

/**
 * Inicializar servicios
 */
const httpInit = () => dispatch =>
  dispatch({
    type: HTTP_INIT,
    payload: {},
  })

export { httpGet, httpPost, httpInit }
