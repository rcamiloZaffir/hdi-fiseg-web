// Login
export const SET_LOGIN = 'SET_LOGIN'
export const GET_LOGIN = 'GET_LOGIN'
export const REQ_LOGIN = 'REQ_LOGIN'
export const IS_LOGGED = 'IS_LOGGED'

// Notification
export const LAUNCH_MESSAGE = 'LAUNCH_MESSAGE'
export const LAUNCH_MODAL = 'LAUNCH_MODAL'
export const LAUNCH_NOTIFY = 'LAUNCH_NOTIFY'
export const CLOSE_HANDLER = 'CLOSE_HANDLER'
export const IS_LOADING = 'IS_LOADING'
export const IS_LOADED = 'IS_LOADED'
export const OPEN_MODAL = 'OPEN_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'

// Form
export const FORM_INIT = 'FORM_INIT'
export const FORM_VALIDATE = 'FORM_VALIDATE'
export const FORM_CHANGE = 'FORM_CHANGE'
export const FORM_RESET = 'FORM_RESET'
export const FORM_UPDATE = 'FORM_UPDATE'
export const FORM_PUT = 'PUT'

// Http
export const HTTP_INIT = 'HTTP_INIT'
export const HTTP_GET = 'HTTP_GET'
export const HTTP_POST = 'HTTP_POST'

// Permission
export const GET_PERMISSION = 'GET_PERMISSION'
