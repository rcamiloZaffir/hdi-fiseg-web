import Cookies from 'js-cookie'
import { POST } from '@utils/constants'
import { LOGIN } from '@utils/services'
import { ClientHttpRequest } from '@utils/httpService'
import { launchMessage } from './errorHandler'
import { REQ_LOGIN } from './actionTypes'
import { loaded, loading } from './spinner'

export const reqLogin = () => ({
  type: REQ_LOGIN,
})

export const login = form => dispatch => {
  dispatch(loading())

  ClientHttpRequest({
    path: LOGIN,
    method: POST,
    params: form,
  })
    .then(data => {
      dispatch(loaded())

      if (data.response) {
        const { token, username, email } = data.response.data
        Cookies.set('token', token)
        Cookies.set('username', username)
        Cookies.set('email', email)
        window.location.reload()
      } else if (data.error) {
        const { message } = data.error.response.data
        dispatch(
          launchMessage({
            type: 'modal',
            level: 'error',
            message:
              message || 'Error al intentar acceder a los servicios de login',
          })
        )
      }
    })
    .catch(error => {
      dispatch(loaded())
      dispatch(
        launchMessage({
          type: 'modal',
          level: 'error',
          message: error.message,
        })
      )
    })
}
