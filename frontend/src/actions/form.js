/**
 * Actions de formulario
 * @module actions/form
 */
import {
  FORM_INIT,
  FORM_CHANGE,
  FORM_VALIDATE,
  FORM_RESET,
  FORM_UPDATE,
  FORM_PUT,
} from './actionTypes'

/**
 * Action para inicializar formulario
 * @param {JSON} form Json de formulario
 */
const formInit = (form = {}) => dispatch => {
  dispatch({
    type: FORM_INIT,
    payload: {
      form,
    },
  })
}

/**
 * Action para cambiar formulario
 * @param {String} name nombre del formulario
 * @param {*} value valor a cambiar del formulario
 */
const formChange = (name, value) => dispatch => {
  dispatch({
    type: FORM_CHANGE,
    payload: {
      name,
      value,
    },
  })
}

/**
 * Avtion para validar formulario con joy
 * @param {Boolean} validate valor para inicializar validacion de formulario
 */
const formValidate = validate => dispatch => {
  dispatch({
    type: FORM_VALIDATE,
    payload: {
      validate,
    },
  })
}

/**
 * Actions para reiniciar formulario
 * @param {JSON} form JSON para reiniciar formulario
 */
const formReset = (form = {}) => dispatch => {
  dispatch({
    type: FORM_RESET,
    payload: {
      validate: form.validate,
      hasFeedback: form.hasFeedback,
      form: form.form,
    },
  })
}

/**
 * Actions para actualizar formulario
 * @param {JSON} form Json para actualizar formulario
 */
const formUpdate = (form = {}) => dispatch => {
  dispatch({
    type: FORM_UPDATE,
    payload: {
      form,
    },
  })
}

/**
 * Actions para agregar un valor al formulario
 * @param {String} name  nombre de la propiedad
 * @param {*} value valor asignado
 */
const formPut = (name, value) => dispatch => {
  dispatch({
    type: FORM_PUT,
    payload: {
      name,
      value,
    },
  })
}

export { formInit, formChange, formValidate, formReset, formUpdate, formPut }
