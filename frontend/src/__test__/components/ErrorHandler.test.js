import React from 'react'
import '../index'
import { mount } from 'enzyme'
import { createStore } from 'redux'
import reducers from '@reducers'
import { Provider } from 'react-redux'
import ErrorHandler from '../../components/ErrorHandler'

const getWrapper = (type, level) =>
  mount(
    <Provider store={createStore(reducers, {})}>
      <ErrorHandler
        errorHandler={{
          level,
          loading: true,
          message: '',
          open: {
            openedState: false,
            openedProps: true,
          },
          statusCode: 400,
          title: '',
          type,
        }}
      />
    </Provider>
  )
let wrapper

describe('ErrorHandler component', () => {
  it('test message', () => {
    wrapper = getWrapper('message', 'success')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('message', 'warning')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('message', 'error')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('message', 'info')
    expect(wrapper).not.toBeNull()
  })

  it('test modal', () => {
    wrapper = getWrapper('modal', 'success')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('modal', 'warning')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('modal', 'error')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('modal', 'info')
    expect(wrapper).not.toBeNull()
  })

  it('test notification', () => {
    wrapper = getWrapper('notification', 'success')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('notification', 'warning')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('notification', 'error')
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper('notification', 'info')
    expect(wrapper).not.toBeNull()
  })
})
