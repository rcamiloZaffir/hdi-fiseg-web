import React from 'react'
import '../index'
import { shallow, mount } from 'enzyme'
import { GraphMonth } from '@components'
import moment from 'moment'

let wrapper

describe('GraphMonth component', () => {
  it('test', () => {
    wrapper = shallow(
      <GraphMonth
        className="mt-4"
        name="GraphMonth"
        form={{
          month: moment().month(),
          year: moment().year(),
        }}
        data={{
          total: 6000,
          individual: 3000,
          flotilla: 3000,
          graph: {
            individualResidente: 1400,
            individualTurista: 1600,
            flotillaResidente: 3000,
          },
        }}
        onExportPdf={() => {}}
        onSendEmail={() => {}}
        onRefresh={() => {}}
        onChange={() => {}}
      />
    )
    expect(wrapper.props()).not.toBeNull()
  })
})

describe('GraphMonth component null', () => {
  it('test', () => {
    wrapper = shallow(
      <GraphMonth
        className="mt-4"
        name="GraphMonth"
        form={{
          month: moment().month(),
          year: moment().year(),
        }}
        onExportPdf={() => {}}
        onSendEmail={() => {}}
        onRefresh={() => {}}
        onChange={() => {}}
        data={{
          total: 0,
          individual: 0,
          flotilla: 0,
          graph: {
            individualResidente: 0,
            individualTurista: 0,
            flotillaResidente: 0,
          },
        }}
      />
    )
    expect(wrapper.props()).not.toBeNull()
  })
})

describe('GraphMonth simulate', () => {
  it('test', () => {
    wrapper = mount(
      <GraphMonth
        className="mt-4"
        name="GraphMonth"
        form={{
          month: moment().month(),
          year: moment().year(),
        }}
        data={{
          total: 6000,
          individual: 3000,
          flotilla: 3000,
          graph: {
            individualResidente: 1400,
            individualTurista: 1600,
            flotillaResidente: 3000,
          },
        }}
        onExportPdf={() => {}}
        onSendEmail={() => {}}
        onRefresh={() => {}}
        onChange={() => {}}
      />
    )

    // console.info(wrapper.find('Select').find('#mounth').first().debug())

    wrapper
      .find('Select')
      .find('#mounth')
      .at(0)
      .simulate('change', { target: { value: 0, key: 0 } })

    wrapper.find('Button').find('#ver').first().simulate('click')
    wrapper.find('Button').find('#download').first().simulate('click')
    // wrapper.find('Button').find('#email').first().simulate('click')

    expect(wrapper.props()).not.toBeNull()
  })
})

describe('GraphMonth 2', () => {
  it('test', () => {
    wrapper = mount(
      <GraphMonth
        className="mt-4"
        name="GraphMonth"
        form={{
          month: moment().month(),
          year: moment().year(),
        }}
        data={{
          total: 6000,
          individual: 3000,
          flotilla: 3000,
          graph: {
            individualResidente: 1400,
            individualTurista: 1600,
            flotillaResidente: 3000,
          },
        }}
        onExportPdf={() => {}}
        onSendEmail={() => {}}
        onRefresh={() => {}}
        onChange={() => {}}
      />
    )

    wrapper.find('Select').find('#mounth').first().prop('onChange')({
      value: 'abril',
      key: '4',
    })

    expect(wrapper.props()).not.toBeNull()
  })
})
