import React from 'react'
import '../index'
import { mount, shallow } from 'enzyme'
import { Table } from '@components'

describe('Table component', () => {
  it('test', () => {
    const wrapper = shallow(
      <Table
        columns={[
          {
            title: 'Name',
            dataIndex: 'pname',
          },
        ]}
        dataSource={[
          {
            key: 1,
            name: 'Sinuhe',
          },
        ]}
        pagination={{
          current: 5,
          pageSize: 50,
          total: 100,
        }}
      />
    )

    expect(wrapper.props().columns).not.toBeNull()
    expect(wrapper.props().dataSource).not.toBeNull()
    expect(wrapper.props().pagination).not.toBeNull()
  })

  it('test mount', () => {
    const wrapper = mount(
      <Table
        columns={[
          {
            title: 'Name',
            dataIndex: 'pname',
          },
        ]}
        dataSource={[
          {
            key: 1,
            name: 'Sinuhe',
          },
        ]}
        pagination={{
          current: 5,
          pageSize: 50,
          total: 100,
        }}
        onChange={() => {}}
      />
    )

    const link = wrapper.find('svg').first()
    link.simulate('click')
    expect(link).not.toBeNull()
  })
})
