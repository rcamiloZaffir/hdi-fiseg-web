import React from 'react'
import '../index'
import { shallow } from 'enzyme'
import { IconBack } from '@components'

let wrapper

describe('IconBack component', () => {
  it('test', () => {
    wrapper = shallow(<IconBack to="/home" />)
    expect(wrapper.props()).not.toBeNull()
  })
})
