import React from 'react'
import '../index'
import { mount } from 'enzyme'
import { createStore } from 'redux'
import reducers from '@reducers'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { loading } from '@actions/spinner'
import Spinner from '../../components/Spinner'

const middlewares = [thunk]
const mockStoret = configureMockStore(middlewares)
let store

const getWrapper = (mockStore = createStore(reducers, {})) =>
  mount(
    <Provider store={mockStore}>
      <Spinner />
    </Provider>
  )

describe('Spinner component', () => {
  it('test', () => {
    const wrapper = getWrapper()

    expect(wrapper).not.toBeNull()
  })
})

describe('Actions spinner', () => {
  it('test', () => {
    store = mockStoret({
      spinner: {
        loading: true,
      },
    })
    store.dispatch(loading())

    const a = mount(
      <Provider store={store}>
        <Spinner />
      </Provider>
    )

    expect(a).not.toBeNull()
  })
})
