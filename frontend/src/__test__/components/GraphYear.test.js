import React from 'react'
import '../index'
import { shallow } from 'enzyme'
import { GraphYear } from '@components'

let wrapper

describe('GraphYear component', () => {
  it('test', () => {
    wrapper = shallow(
      <GraphYear
        className="mt-4"
        name="GraphYear"
        data={{
          total: 6000,
          totalesEstatus: {
            porcentajeError: 0.05,
            extraidos: 6000,
            procesados: 4000,
            exitosos: 3800,
            reintento: 15,
            error: 200,
          },
          months: [
            {
              monthYear: '202005',
              individualResidente: 1500,
              individualTuristas: 1500,
              flotillaResidente: 2950,
              error: 50,
            },
            {
              monthYear: '202004',
              individualResidente: 1500,
              individualTuristas: 1500,
              flotillaResidente: 2950,
              error: 50,
            },
            {
              monthYear: '202003',
              individualResidente: 1500,
              individualTuristas: 1500,
              flotillaResidente: 2950,
              error: 50,
            },
            {
              monthYear: '202002',
              individualResidente: 1500,
              individualTuristas: 1500,
              flotillaResidente: 2950,
              error: 50,
            },
          ],
        }}
        onExportPdf={() => {}}
        onSendEmail={() => {}}
        onRefresh={() => {}}
      />
    )
    expect(wrapper.props()).not.toBeNull()
  })
})

describe('GraphYear component simulate', () => {
  it('test', () => {
    wrapper = shallow(
      <GraphYear
        className="mt-4"
        name="GraphYear"
        data={{
          total: 6000,
          totalesEstatus: {
            porcentajeError: 0.05,
            extraidos: 6000,
            procesados: 4000,
            exitosos: 3800,
            reintento: 15,
            error: 200,
          },
          months: [
            {
              monthYear: '202005',
              individualResidente: 1500,
              individualTuristas: 1500,
              flotillaResidente: 2950,
              error: 50,
            },
            {
              monthYear: '202004',
              individualResidente: 1500,
              individualTuristas: 1500,
              flotillaResidente: 2950,
              error: 50,
            },
            {
              monthYear: '202003',
              individualResidente: 1500,
              individualTuristas: 1500,
              flotillaResidente: 2950,
              error: 50,
            },
            {
              monthYear: '202002',
              individualResidente: 1500,
              individualTuristas: 1500,
              flotillaResidente: 2950,
              error: 50,
            },
          ],
        }}
        onExportPdf={() => {}}
        onSendEmail={() => {}}
        onRefresh={() => {}}
      />
    )

    // console.info(wrapper.find('Select').find('#mounth').debug())

    wrapper.find('Button').find('#download').first().simulate('click')
    // wrapper.find('Button').find('#email').first().simulate('click')

    expect(wrapper.props()).not.toBeNull()
  })
})

describe('GraphYear component null', () => {
  it('test', () => {
    wrapper = shallow(
      <GraphYear
        className="mt-4"
        name="GraphYear"
        onExportPdf={() => {}}
        onSendEmail={() => {}}
        onRefresh={() => {}}
      />
    )
    expect(wrapper.props()).not.toBeNull()
  })
})
