import React from 'react'
import '../index'
import { shallow } from 'enzyme'
import { Slider } from '@components'
import { HDI_FILTERS } from '@utils/constants'

describe('Slider component', () => {
  it('test', () => {
    let wrapper = shallow(<Slider statusName={HDI_FILTERS[0].text} />)
    expect(wrapper.props()).not.toBeNull()

    wrapper = shallow(<Slider statusName={HDI_FILTERS[1].text} />)
    expect(wrapper.props()).not.toBeNull()

    wrapper = shallow(<Slider statusName={HDI_FILTERS[2].text} />)
    expect(wrapper.props()).not.toBeNull()

    wrapper = shallow(<Slider statusName={HDI_FILTERS[3].text} />)
    expect(wrapper.props()).not.toBeNull()

    wrapper = shallow(<Slider statusName={HDI_FILTERS[4].text} />)
    expect(wrapper.props()).not.toBeNull()

    wrapper = shallow(<Slider statusName={HDI_FILTERS[5].text} />)
    expect(wrapper.props()).not.toBeNull()

    wrapper = shallow(<Slider statusName={HDI_FILTERS[6].text} />)
    expect(wrapper.props()).not.toBeNull()
  })
})
