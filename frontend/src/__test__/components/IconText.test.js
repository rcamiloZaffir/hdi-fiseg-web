import React from 'react'
import '../index'
import { shallow, mount } from 'enzyme'
import { IconText } from '@components'
import ShowChartIcon from '@material-ui/icons/ShowChart'

let wrapper

describe('IconText component', () => {
  it('test', () => {
    wrapper = shallow(
      <IconText className="" icon={ShowChartIcon} data={11} title="Exitosos" />
    )
    expect(wrapper.props()).not.toBeNull()

    wrapper = shallow(
      <IconText className="" img="car" data={11} title="Exitosos" />
    )
    expect(wrapper.props()).not.toBeNull()

    wrapper = shallow(
      <IconText className="" img="car2" data={11} title="Exitosos" />
    )
    expect(wrapper.props()).not.toBeNull()
  })

  it('car', () => {
    wrapper = mount(
      <IconText className="" img="car" data={11} title="Exitosos" />
    )
    expect(wrapper.props()).not.toBeNull()
  })

  it('car2', () => {
    wrapper = mount(
      <IconText className="" img="car2" data={11} title="Exitosos" />
    )
    expect(wrapper.props()).not.toBeNull()
  })

  it('nom image', () => {
    wrapper = mount(<IconText className="" img="" data={11} title="Exitosos" />)
    expect(wrapper.props()).not.toBeNull()
  })
})
