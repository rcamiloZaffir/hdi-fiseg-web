import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import {
  closeNotification,
  launchMessage,
  launchModal,
  launchNotify,
} from '@actions/errorHandler'
import {
  CLOSE_HANDLER,
  LAUNCH_MESSAGE,
  LAUNCH_MODAL,
  LAUNCH_NOTIFY,
} from '@actions/actionTypes'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
let store

describe('Actions errorHandler', () => {
  it('test', () => {
    store = mockStore({})
    store.dispatch(closeNotification(''))
    expect(store.getActions()).toEqual([{ type: CLOSE_HANDLER }])

    store = mockStore({})
    store.dispatch(closeNotification())
    expect(store.getActions()).toEqual([{ type: CLOSE_HANDLER }])

    store = mockStore({})
    store.dispatch(launchMessage({}))
    expect(store.getActions()).toEqual([{ type: LAUNCH_MESSAGE, payload: {} }])

    store = mockStore({})
    store.dispatch(launchModal({}))
    expect(store.getActions()).toEqual([
      { type: LAUNCH_MODAL, payload: {}, route: null },
    ])

    store = mockStore({})
    store.dispatch(launchNotify({}))
    expect(store.getActions()).toEqual([
      { type: LAUNCH_NOTIFY, payload: {}, route: null },
    ])
  })
})
