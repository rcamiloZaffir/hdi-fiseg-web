import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import {
  formInit,
  formChange,
  formValidate,
  formReset,
  formUpdate,
  formPut,
} from '@actions/form'
import {
  FORM_INIT,
  FORM_CHANGE,
  FORM_VALIDATE,
  FORM_RESET,
  FORM_PUT,
} from '@actions/actionTypes'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
let store

describe('Actions form', () => {
  it('test', () => {
    store = mockStore({})
    store.dispatch(formInit({}))
    expect(store.getActions()).toEqual([
      {
        type: FORM_INIT,
        payload: {
          form: {},
        },
      },
    ])

    store = mockStore({})
    store.dispatch(formInit())
    expect(store.getActions()).toEqual([
      {
        type: FORM_INIT,
        payload: {
          form: {},
        },
      },
    ])

    store = mockStore({})
    store.dispatch(formChange('sinuhe', '20'))
    expect(store.getActions()).toEqual([
      {
        type: FORM_CHANGE,
        payload: {
          name: 'sinuhe',
          value: '20',
        },
      },
    ])

    store = mockStore({})
    store.dispatch(formValidate(true))
    expect(store.getActions()).toEqual([
      { type: FORM_VALIDATE, payload: { validate: true } },
    ])

    store = mockStore({})
    store.dispatch(
      formReset({
        validate: false,
        hasFeedback: false,
        form: {},
      })
    )
    expect(store.getActions()).toEqual([
      {
        type: FORM_RESET,
        payload: {
          validate: false,
          hasFeedback: false,
          form: {},
        },
      },
    ])

    store = mockStore({})
    store.dispatch(formReset())
    expect(store.getActions()).toEqual([
      {
        type: FORM_RESET,
        payload: {
          validate: undefined,
          hasFeedback: undefined,
          form: undefined,
        },
      },
    ])

    store = mockStore({})
    store.dispatch(formUpdate({}))
    expect(store.getActions()).not.toBeNull()

    store = mockStore({})
    store.dispatch(formUpdate())
    expect(store.getActions()).not.toBeNull()

    store = mockStore({})
    store.dispatch(formPut('sinuhe', '20'))
    expect(store.getActions()).toEqual([
      { type: FORM_PUT, payload: { name: 'sinuhe', value: '20' } },
    ])
  })
})
