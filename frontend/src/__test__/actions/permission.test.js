import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import { getPermission } from '@actions/permission'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
let store

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    return {
      response: {
        data: {},
        status: 200,
        options,
      },
    }
  },
}))

describe('Actions permission OK', () => {
  it('test', () => {
    store = mockStore({})
    store.dispatch(getPermission())
    expect(store.getActions()).not.toBeNull()
  })
})

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    return {
      response: {
        data: {},
        status: 400,
        options,
      },
    }
  },
}))

describe('Actions permission ERROR', () => {
  it('test', () => {
    store = mockStore({})
    store.dispatch(getPermission())
    expect(store.getActions()).not.toBeNull()
  })
})
