import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import { login } from '@actions/login'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
let store

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    return {
      response: {
        data: {},
        options,
      },
    }
  },
}))

describe('Actions login OK', () => {
  it('test', () => {
    store = mockStore({})

    store.dispatch(
      login({
        username: '',
        password: '',
      })
    )

    expect(store.getActions()).not.toBeNull()
  })
})
