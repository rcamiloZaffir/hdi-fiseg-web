import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import { httpGet, httpPost, httpInit } from '@actions/http'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
let store

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    return {
      error: {
        response: {
          data: {},
          options,
        },
      },
    }
  },
}))

describe('Actions http2', () => {
  it('httpGet', () => {
    store = mockStore({})
    store.dispatch(
      httpGet('http://localhost:8080/dashboard/graph', {
        month: '08',
        year: 2020,
      })
    )
    expect(store.getActions()).not.toBeNull()
  })

  it('httpPost', () => {
    store = mockStore({})
    store.dispatch(httpPost('localhost:8080/mailto'))
    expect(store.getActions()).not.toBeNull()
  })

  it('httpInit', () => {
    store = mockStore({})
    store.dispatch(httpInit({}))
    expect(store.getActions()).not.toBeNull()
  })
})
