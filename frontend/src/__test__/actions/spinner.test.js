import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import { loading, loaded } from '@actions/spinner'
import { IS_LOADING, IS_LOADED } from '@actions/actionTypes'

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)
let store

describe('Actions spinner', () => {
  it('test', () => {
    store = mockStore({})
    store.dispatch(loading())
    expect(store.getActions()).toEqual([{ type: IS_LOADING }])

    store = mockStore({})
    store.dispatch(loaded())
    expect(store.getActions()).toEqual([{ type: IS_LOADED }])
  })
})
