import { IS_LOADING, IS_LOADED } from '@actions/actionTypes'
import spinner from '../../reducers/spinner'

describe('Reducers spinner', () => {
  it('test', () => {
    expect(spinner({}, { type: IS_LOADING })).toEqual({ loading: true })

    expect(spinner({}, { type: IS_LOADED })).toEqual({ loading: false })
  })
})
