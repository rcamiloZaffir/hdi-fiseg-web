import { OPEN_MODAL, CLOSE_MODAL } from '@actions/actionTypes'
import modal from '../../reducers/modal'

describe('Reducers modal', () => {
  it('test', () => {
    expect(modal({}, { type: OPEN_MODAL })).toEqual({ open: true })
    expect(modal({}, { type: CLOSE_MODAL })).toEqual({ open: false })
  })
})
