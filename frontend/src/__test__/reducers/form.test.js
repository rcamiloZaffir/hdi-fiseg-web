import {
  FORM_INIT,
  FORM_CHANGE,
  FORM_VALIDATE,
  FORM_RESET,
  FORM_UPDATE,
  FORM_PUT,
} from '@actions/actionTypes'
import form from '../../reducers/form'

describe('Reducers form', () => {
  it('test', () => {
    expect(
      form(
        {},
        {
          type: FORM_INIT,
          payload: {
            form: {},
          },
        }
      )
    ).not.toBeNull()

    expect(
      form(
        {},
        {
          type: FORM_UPDATE,
          payload: {
            form: {},
          },
        }
      )
    ).not.toBeNull()

    expect(
      form(
        {},
        {
          type: FORM_CHANGE,
          payload: {
            form: {},
          },
        }
      )
    ).not.toBeNull()

    expect(
      form(
        {},
        {
          type: FORM_VALIDATE,
          payload: {
            form: {},
          },
        }
      )
    ).not.toBeNull()

    expect(
      form(
        {},
        {
          type: FORM_RESET,
          payload: {
            form: {},
          },
        }
      )
    ).not.toBeNull()

    expect(
      form(
        {},
        {
          type: FORM_PUT,
          payload: {
            form: {},
          },
        }
      )
    ).not.toBeNull()
  })
})
