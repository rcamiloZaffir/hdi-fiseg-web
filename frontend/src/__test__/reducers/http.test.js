import { HTTP_GET, HTTP_POST, HTTP_INIT } from '@actions/actionTypes'
import http from '../../reducers/http'

describe('Reducers http', () => {
  it('test', () => {
    expect(
      http(
        {},
        {
          type: HTTP_INIT,
        }
      )
    ).toEqual({})

    expect(
      http(
        {},
        {
          type: HTTP_GET,
          payload: {
            path: 'path',
            data: {},
          },
        }
      )
    ).toEqual({ path: {} })

    expect(
      http(
        {},
        {
          type: HTTP_POST,
          payload: {
            path: 'path',
            data: {},
          },
        }
      )
    ).toEqual({ path: {} })
  })
})
