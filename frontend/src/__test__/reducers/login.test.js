import {
  SET_LOGIN,
  GET_LOGIN,
  REQ_LOGIN,
  IS_LOGGED,
} from '@actions/actionTypes'
import login from '../../reducers/login'

describe('Reducers login', () => {
  it('test', () => {
    expect(
      login(
        {},
        {
          type: SET_LOGIN,
          payload: 'sinuhe',
        }
      )
    ).toEqual({ login: 'sinuhe' })

    expect(
      login(
        {},
        {
          type: GET_LOGIN,
          payload: 'sinuhe',
        }
      )
    ).toEqual({ user: 'sinuhe' })

    expect(login({}, { type: REQ_LOGIN })).toEqual({ reqLogin: true })

    expect(login({}, { type: IS_LOGGED })).toEqual({ reqLogin: false })
  })
})
