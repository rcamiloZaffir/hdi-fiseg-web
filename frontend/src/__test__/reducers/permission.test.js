import { GET_PERMISSION } from '@actions/actionTypes'
import permission from '../../reducers/permission'

describe('Reducers permission', () => {
  it('test', () => {
    expect(
      permission(
        {},
        {
          type: GET_PERMISSION,
          payload: {
            menu: [],
            url: [],
          },
        }
      )
    ).not.toBeNull()

    expect(permission({}, { type: null })).toBeNull()
  })
})
