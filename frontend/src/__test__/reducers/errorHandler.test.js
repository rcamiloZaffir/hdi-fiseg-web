import {
  CLOSE_HANDLER,
  IS_LOADING,
  LAUNCH_MESSAGE,
  LAUNCH_MODAL,
  LAUNCH_NOTIFY,
} from '@actions/actionTypes'
import errorHandler from '../../reducers/errorHandler'

describe('Reducers errorHandler', () => {
  it('test', () => {
    expect(
      errorHandler(
        {},
        {
          type: LAUNCH_MESSAGE,
          payload: {},
        }
      )
    ).not.toBeNull()

    expect(
      errorHandler(
        {},
        {
          type: LAUNCH_MODAL,
          payload: {},
        }
      )
    ).not.toBeNull()

    expect(
      errorHandler(
        {},
        {
          type: LAUNCH_NOTIFY,
          payload: {},
        }
      )
    ).not.toBeNull()

    expect(
      errorHandler(
        {},
        {
          type: CLOSE_HANDLER,
          payload: {},
        }
      )
    ).not.toBeNull()

    expect(
      errorHandler(
        {},
        {
          type: IS_LOADING,
          payload: {},
        }
      )
    ).not.toBeNull()
  })
})
