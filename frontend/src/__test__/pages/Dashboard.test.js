import { act } from 'react-dom/test-utils'
import { getWrapper } from '../index'
import Dashboard from '../../pages/Dashboard'
import 'jest-canvas-mock'

let wrapper

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    const { path } = options

    const DASHBOARD_GRAPH = '/dashboard/graph'
    const DASHBOARD_GRAPH_HISTORY = '/dashboard/graph/history'
    const DASHBOARD_GRAPH_PROCESSING_HISTORY =
      '/dashboard/graph/processingHistory'
    const MAIL_TO = '/mailto'

    switch (path) {
      case DASHBOARD_GRAPH:
        return {
          response: {
            data: {
              total: 6818,
              individual: 6818,
              flotilla: 0,
              graph: {
                individualResidente: 6706,
                individualTurista: 106,
                flotillaResidente: 0,
              },
            },
          },
        }

      case DASHBOARD_GRAPH_HISTORY:
        return {
          response: {
            data: {
              total: 6839,
              totalesEstatus: {
                porcentajeError: 0.014622020763269484,
                extraidos: 1,
                procesados: 1,
                exitosos: 1,
                reintentos: 6835,
                error: 1,
              },
              months: [
                {
                  monthYear: '202010',
                  individualResidente: 10,
                  individualTuristas: 0,
                  flotillaResidente: 0,
                  error: 1,
                },
                {
                  monthYear: '202009',
                  individualResidente: 6706,
                  individualTuristas: 106,
                  flotillaResidente: 0,
                  error: 0,
                },
                {
                  monthYear: '202008',
                  individualResidente: 11,
                  individualTuristas: 0,
                  flotillaResidente: 0,
                  error: 0,
                },
                {
                  monthYear: '202007',
                  individualResidente: 0,
                  individualTuristas: 0,
                  flotillaResidente: 0,
                  error: 0,
                },
              ],
            },
          },
        }

      case DASHBOARD_GRAPH_PROCESSING_HISTORY:
        return {
          response: {
            data: {
              historia: [
                {
                  fechaHora: '2020-10-09T20:28:01.417',
                  estatus: 'Ok',
                  descripcion: 'Autos residentes',
                  registros: 278,
                },
                {
                  fechaHora: '2020-10-09T20:06:01.463',
                  estatus: 'Ok',
                  descripcion: 'Autos residentes',
                  registros: 278,
                },
                {
                  fechaHora: '2020-10-09T19:44:01.487',
                  estatus: 'Ok',
                  descripcion: 'Autos residentes',
                  registros: 278,
                },
                {
                  fechaHora: '2020-10-09T19:30:01.373',
                  estatus: 'Ok',
                  descripcion: 'Autos residentes',
                  registros: 278,
                },
                {
                  fechaHora: '2020-10-09T16:06:00.17',
                  estatus: 'Error',
                  descripcion: 'Autos residentes',
                  registros: 278,
                },
              ],
            },
          },
        }

      case MAIL_TO:
        return { response: { data: {} } }
    }
  },
}))

describe('Dashboard page Table', () => {
  test('test', async () => {
    await act(async () => {
      wrapper = getWrapper(Dashboard)
    })
    wrapper.update()

    // test sort
    for (let i = 0; i < 4; i++) {
      wrapper
        .find('Table')
        .find('.ant-table-thead tr')
        .childAt(i)
        .simulate('click')
    }

    wrapper.mount('GraphMonth')
    wrapper.find('Select').find('#mounth').at(0).simulate('change', {})
    wrapper.find('Button').find('#ver').first().simulate('click')
    wrapper.find('Button').find('#download').first().simulate('click')
    // wrapper.find('Button').find('#email').first().simulate('click')

    wrapper.mount('GraphYear')
    wrapper.find('Button').find('#download').first().simulate('click')
    // wrapper.find('Button').find('#email').first().simulate('click')

    expect(wrapper).not.toBeNull()
  })
})
