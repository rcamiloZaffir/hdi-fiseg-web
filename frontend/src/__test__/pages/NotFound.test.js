import { getWrapper } from '../index'
import NotFound from '../../pages/NotFound'

describe('NotFound page', () => {
  it('test', () => {
    const wrapper = getWrapper(NotFound)

    expect(wrapper).not.toBeNull()
  })
})
