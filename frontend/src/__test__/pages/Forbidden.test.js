import { getWrapper } from '../index'
import Forbidden from '../../pages/Forbidden'

const wrapper = getWrapper(Forbidden)

describe('Forbidden page', () => {
  it('test', () => {
    expect(wrapper).not.toBeNull()
  })
})
