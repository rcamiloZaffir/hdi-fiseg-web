import { getWrapper } from '../index'
import Login from '../../pages/Login'

describe('Login page', () => {
  it('test', () => {
    const wrapper = getWrapper(Login)

    wrapper.find('Input').first().prop('onChange')({
      target: {
        name: 'password',
        value: 'usernae',
      },
    })

    wrapper.find('Input').find('#password').first().prop('onChange')({
      target: {
        namer: 'email',
        value: 'password',
      },
    })

    wrapper.find('Button').first().simulate('click')

    expect(wrapper).not.toBeNull()
  })
})
