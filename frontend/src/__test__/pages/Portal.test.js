import { getWrapper } from '../index'
import Portal from '../../pages/Portal'
import TopMenu from '../../pages/Portal/TopMenu'
import UserMenu from '../../pages/Portal/UserMenu'

jest.mock('react-router-dom', () => ({
  useLocation: jest.fn().mockReturnValue({
    pathname: '/another-route',
    search: '',
    hash: '',
    state: null,
    key: '5nvxpbdafa',
  }),
  useHistory: () => ({
    push: jest.fn(),
  }),
}))

let wrapper

describe('Portal page', () => {
  it('test', () => {
    wrapper = getWrapper(Portal)
    expect(wrapper).not.toBeNull()
  })

  it('Search test', () => {
    wrapper = getWrapper(Portal)

    // console.info(wrapper.debug())

    expect(wrapper).not.toBeNull()
  })

  it('TopMenu test', () => {
    wrapper = getWrapper(TopMenu)
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper(TopMenu)
    const link = wrapper.find('li').first()
    link.simulate('click')

    wrapper.find('Menu').first().prop('onClick')({
      key: 'Dashboard',
    })

    expect(link).not.toBeNull()
  })

  it('UserMenu test', () => {
    wrapper = getWrapper(UserMenu)
    expect(wrapper).not.toBeNull()

    wrapper = getWrapper(UserMenu)
    const link = wrapper.find('svg').first()
    link.simulate('click')

    wrapper.find('#logout').first().simulate('click')

    expect(link).not.toBeNull()
  })
})
