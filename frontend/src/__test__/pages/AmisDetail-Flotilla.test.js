import { act } from 'react-dom/test-utils'
import { getWrapper } from '../index'
import AmisDetail from '../../pages/AmisDetail'

let wrapper

global.window = Object.create(window)

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    const { path } = options

    const AMIS_MOVIMIENTOS = '/amis/movimientos'
    const AMIS_MOVIMIENTOS_VEHICLES = '/amis/movimientos/vehicles'
    const AMIS_MOVIMIENTOS_FLOTILLA_DETALLE =
      '/amis/movimientos/flotilla/detalles'

    switch (path) {
      case `${AMIS_MOVIMIENTOS}/10620`:
        return {
          response: {
            data: {
              id: '10620',
              status: 'process',
              date: '20200511',
              policyType: 4214,
              insuranceData: {
                typeInsurance: 4,
                policyNumber: 'DVNIOFSVSFVSF',
                issueDate: '20201007',
                movementType: 1,
                startValidity: '20201007',
                finalValidity: '20201007',
              },
              client: {
                lastName: 'Maceda',
                secondLastName: 'Bouchan',
                name: 'Sinuhe',
                curp: 'DFDCEGR7481',
                rfc: 'DFDCEGR7481',
              },
              beneficiary: {
                lastName: 'Maceda',
                secondLastName: 'Bouchan',
                name: 'Sinuhe',
                curp: 'DFDCEGR7481',
                rfc: 'DFDCEGR7481',
              },
              salesChannel: {
                salesChannel: 2,
                agency: 3214,
                agent: 90399,
                locationInsured: 544,
                agentLocation: 544,
              },
              coverage: [
                { id: 1, insuranceSumId: 1, insuranceSum: 55555, status: 1 },
                { id: 1, insuranceSumId: 1, insuranceSum: 55555, status: 1 },
                { id: 1, insuranceSumId: 1, insuranceSum: 55555, status: 1 },
              ],
            },
          },
        }

      case `${AMIS_MOVIMIENTOS_VEHICLES}/10620`:
        return {
          response: {
            data: {
              total: 2,
              data: [
                {
                  vin: 'MVB2EDFDFDRSFS',
                  use: 1,
                  required: 0,
                  model: 2001,
                  licensePlate: '20GFH',
                  inciso: 16,
                  brand: 2489,
                  client2: 'Ricardo Esparza',
                  Beneficiary: 'Ricardo Esparza',
                },
                {
                  vin: 'MVB2EDF23FRSFS',
                  use: 1,
                  required: 0,
                  model: 2001,
                  licensePlate: '21GFH',
                  inciso: 16,
                  brand: 2489,
                  client2: 'Ricardo Esparza',
                  beneficiary: 'Ricardo Esparza',
                },
              ],
            },
          },
        }

      case `${AMIS_MOVIMIENTOS_FLOTILLA_DETALLE}/?movimientoId=10620&vin=MVB2EDFDFDRSFS?`:
        return {
          response: {
            data: {
              client2: {
                ApellidoMaterno: 'Perez',
                ApellidoPaterno: 'Jimenez',
                nombre: 'Jose',
                curp: 'JJPP901212LJSDD3PO',
                rfc: 'JJPP901212',
              },
              beneficiary: {
                lastName: 'Maceda',
                secondLastName: 'Bouchan',
                name: 'Sinuhe',
                curp: 'DFDCEGR7481',
                rfc: 'DFDCEGR7481',
              },
              coverage: [
                { id: 1, insuranceSumId: 1, insuranceSum: 55555, status: 1 },
                {
                  id: 2,
                  insuranceSumId: 444,
                  insuranceSum: 55554445,
                  status: 1,
                },
                {
                  id: 3,
                  insuranceSumId: 1,
                  insuranceSum: 554541555,
                  status: 1,
                },
              ],
            },
          },
        }
    }
  },
}))

describe('AmisDetail Flotilla', () => {
  Object.defineProperty(window, 'location', {
    value: {
      search: {
        movimientoId: 10620,
        //
        insureType: 2,
        date: '20200913',
        status: 'En proceso',

        dateInit: '20201001',
        dateEnd: '20201001',
        // query='',
        currentPage: 1,
        currentPageSize: 15,
        filter: { status: [] },
      },
    },
  })

  test('test', async () => {
    await act(async () => {
      wrapper = getWrapper(AmisDetail)
    })
    wrapper.update()

    // test sort

    for (let i = 0; i < 10; i++) {
      wrapper
        .find('Table')
        .at(1)
        .find('.ant-table-thead tr')
        .childAt(i)
        .simulate('click')
    }

    expect(wrapper).not.toBeNull()
  })
})
