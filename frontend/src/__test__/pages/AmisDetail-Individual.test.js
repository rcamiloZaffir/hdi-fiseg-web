import { act } from 'react-dom/test-utils'
import { getWrapper } from '../index'
import AmisDetail from '../../pages/AmisDetail'

let wrapper

global.window = Object.create(window)

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    const { path } = options

    const AMIS_MOVIMIENTOS = '/amis/movimientos'

    switch (path) {
      case `${AMIS_MOVIMIENTOS}/10617`:
        return {
          response: {
            data: {
              id: 4,
              client: {
                lastName: 'ARZAGA',
                secondLastName: 'URIBE',
                name: 'ALEJANDRO',
                curp: null,
                rfc: 'AAUA640424FT',
              },
              beneficiary: {
                apellidoPaterno: 'ARZAGA',
                apellidoMaterno: 'URIBE',
                nombre: 'ALEJANDRO',
                curp: null,
                rfc: null,
              },
              insuranceData: {
                typeInsurance: 326,
                policyNumber: '245-458',
                issueDate: '2020-10-02 15:12:45.0',
                movementType: 104,
                startValidity: '2020-09-11 00:00:00.0',
                finalValidity: '2020-10-22 00:00:00.0',
              },
              coverage: [
                {
                  id: 1,
                  insuranceSumId: null,
                  insuranceSum: null,
                  status: 3835,
                },
                {
                  id: 6452,
                  insuranceSumId: null,
                  insuranceSum: null,
                  status: 3835,
                },
              ],
              salesChannel: {
                salesChannel: 3835,
                agency: null,
                agent: '61491',
                locationInsured: null,
                locationAgent: null,
              },
              date: '2020-10-02 15:12:45.0',
              policyType: 1,
            },
          },
        }
    }
  },
}))

describe('AmisDetail Individual', () => {
  Object.defineProperty(window, 'location', {
    value: {
      search: {
        movimientoId: 10617,
        //
        insureType: 1,
        date: '20200913',
        status: 'En proceso',

        dateInit: '20201001',
        dateEnd: '20201001',
        // query='',
        currentPage: 1,
        currentPageSize: 15,
        filter: { status: [] },
      },
    },
  })

  test('test', async () => {
    await act(async () => {
      wrapper = getWrapper(AmisDetail)
    })
    wrapper.update()

    // test sort

    for (let i = 0; i < 4; i++) {
      wrapper
        .find('Table')
        .at(1)
        .find('.ant-table-thead tr')
        .childAt(i)
        .simulate('click')
    }

    expect(wrapper).not.toBeNull()
  })
})
