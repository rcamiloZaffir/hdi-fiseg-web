import moment from 'moment'
// import { act } from 'react-dom/test-utils'
import { getWrapper } from '../index'
import Amis from '../../pages/Amis'

let wrapper

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    const { path } = options

    const AMIS_SEARCH = '/amis/search'

    switch (path) {
      case AMIS_SEARCH:
        return {
          response: {
            data: {
              total: 8,
              data: [
                {
                  policy: '245-458',
                  agent: '61491',
                  insureType: '1',
                  vin: '2T1BR32E26C658328',
                  bussinessName: 'ALEJANDRO ARZAGA URIBE',
                  date: '20201002',
                  status: 'Exito',
                  movimientoId: 4,
                  vehiculoId: 6452,
                },
                {
                  policy: '248-1080',
                  agent: '60137',
                  insureType: '1',
                  vin: 'U6YPC3AA1GL683775',
                  bussinessName: 'MARIA ESTHER GRANILLO MEDELLIN',
                  date: '20201003',
                  status: 'En proceso',
                  movimientoId: 6,
                  vehiculoId: 3,
                },
                {
                  policy: '248-1080',
                  agent: '60137',
                  insureType: '1',
                  vin: 'U6YPC3AA1GL683775',
                  bussinessName: 'MARIA ESTHER GRANILLO MEDELLIN',
                  date: '20201003',
                  status: 'En proceso',
                  movimientoId: 6,
                  vehiculoId: 9138,
                },
                {
                  policy: '248-1059',
                  agent: '61263',
                  insureType: '1',
                  vin: '1FDEF3G52AEB23080',
                  bussinessName: 'OSCAR HUMBERTO AVILA ALQUICIRA',
                  date: '20201003',
                  status: 'Error de extracción',
                  movimientoId: 5,
                  vehiculoId: 2,
                },
                {
                  policy: '248-1059',
                  agent: '61263',
                  insureType: '1',
                  vin: '1FDEF3G52AEB23080',
                  bussinessName: 'OSCAR HUMBERTO AVILA ALQUICIRA',
                  date: '20201003',
                  status: 'Error de extracción',
                  movimientoId: 5,
                  vehiculoId: 805,
                },
                {
                  policy: '248-1059',
                  agent: '61263',
                  insureType: '1',
                  vin: '1FDEF3G52AEB23080',
                  bussinessName: 'OSCAR HUMBERTO AVILA ALQUICIRA',
                  date: '20201003',
                  status: 'Error de extracción',
                  movimientoId: 5,
                  vehiculoId: 9137,
                },
                {
                  policy: '248-1059',
                  agent: '61263',
                  insureType: '1',
                  vin: '1FDEF3G52AEB23080',
                  bussinessName: 'OSCAR HUMBERTO AVILA ALQUICIRA',
                  date: '20201003',
                  status: 'Error de extracción',
                  movimientoId: 5,
                  vehiculoId: 9455,
                },
              ],
            },
          },
        }
    }
  },

  buildUrlBase: (path, params, method) => {
    return `/api${path}?${params}${method}`
  },
}))

describe('Amis page', () => {
  it('test', () => {
    wrapper = getWrapper(Amis)

    wrapper.find('RangePicker').first().prop('onCalendarChange')({
      value: new Date(),
    })

    wrapper.find('RangePicker').first().prop('onChange')([moment(), moment()])
    wrapper.find('Button').find('#btnSearch').first().simulate('click')
    wrapper.find('Button').find('#download').first().simulate('click')

    expect(wrapper).not.toBeNull()
  })

  it('test search', () => {
    wrapper = getWrapper(Amis)

    wrapper.find('Input').first().prop('onChange')(
      { target: { value: 'ss78724' } },
      'search'
    )

    expect(wrapper).not.toBeNull()
  })
})

describe('Amis back null', () => {
  it('test', () => {
    global.window = Object.create(window)
    Object.defineProperty(window, 'location', {
      value: {
        search: {
          isBack: 'true',
          dateInit: '20200901',
          dateEnd: '20200901',
          query: '',
          currentPage: 1,
          currentPageSize: 15,
          filter: '{"status":[]}',
        },
      },
    })

    wrapper = getWrapper(Amis)
    expect(wrapper).not.toBeNull()
  })
})

describe('Amis back', () => {
  it('test', () => {
    global.window = Object.create(window)
    Object.defineProperty(window, 'location', {
      value: {
        search: {
          isBack: 'true',
          dateInit: '20200901',
          dateEnd: '20200901',
          query: '1545',
          currentPage: 1,
          currentPageSize: 15,
          filter: '{"status":["En proceso"]}',
        },
      },
    })

    wrapper = getWrapper(Amis)
    expect(wrapper).not.toBeNull()
  })
})

describe('Amis back2', () => {
  it('test', () => {
    global.window = Object.create(window)
    Object.defineProperty(window, 'location', {
      value: {
        search: {
          isBack: 'true',
          dateInit: '20200901',
          dateEnd: '20200901',
          query: 'hi',
          currentPage: 1,
          currentPageSize: 15,
          filter: '{"status":["En proceso"]}',
        },
      },
    })

    wrapper = getWrapper(Amis)
    expect(wrapper).not.toBeNull()
  })
})
