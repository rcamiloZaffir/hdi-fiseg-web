import { act } from 'react-dom/test-utils'
import { getWrapper } from '../index'
import Search from '../../pages/Search'

let wrapper

jest.mock('../../utils/httpService', () => ({
  ClientHttpRequest: async options => {
    const { path } = options

    const searchId = '1GNCS4564165M5'

    const SEARCH = `/search/${searchId}`
    const SEARCH_POLICY_VEHICLES = `/search/policy/vehicles/${searchId}`
    const SEARCH_EXCEL = `/search/excel/${searchId}`
    const SEARCH_MAIL_TO = `/search/mailto/${searchId}`

    switch (path) {
      case SEARCH:
        return {
          response: {
            data: {
              policyId: '1346788964433',
              status: 'cancel',
              vin: '1GNCS4564165M5',
              estatus: 'En proceso',
              fechaRegistro: '20201111',
              insuranceCompany: [
                {
                  insuranceTypeId: 1,
                  policy: '00056878952',
                  dateOfIssue: '20170101',
                  moveType: 2,
                  initValidity: '20180101',
                  endValidity: '20300101',
                },
                {
                  insuranceTypeId: 1,
                  policy: '00056878952',
                  dateOfIssue: '20170101',
                  moveType: 2,
                  initValidity: '20180101',
                  endValidity: '20300101',
                },
                {
                  insuranceTypeId: 1,
                  policy: '00056878952',
                  dateOfIssue: '20170101',
                  moveType: 2,
                  initValidity: '20180101',
                  endValidity: '20300101',
                },
                {
                  insuranceTypeId: 1,
                  policy: '00056878952',
                  dateOfIssue: '20170101',
                  moveType: 2,
                  initValidity: '20180101',
                  endValidity: '20300101',
                },
              ],
              coverage: [
                {
                  id: 1,
                  insuranceSumId: 1,
                  insuranceSum: 55555,
                  status: 1,
                },
                {
                  id: 1,
                  insuranceSumId: 1,
                  insuranceSum: 55555,
                  status: 1,
                },
                {
                  id: 1,
                  insuranceSumId: 1,
                  insuranceSum: 55555,
                  status: 1,
                },
              ],
            },
          },
        }

      case SEARCH_POLICY_VEHICLES:
        return {
          response: {
            data: {
              total: 2,
              data: [
                {
                  vin: 'MVB2EDFDFDRSFS',
                  use: 1,
                  required: 0,
                  model: 2001,
                  licensePlate: '20GFH',
                  inciso: 16,
                  brand: 2489,
                  client2: 'Ricardo Esparza',
                  Beneficiary: 'Ricardo Esparza',
                },
                {
                  vin: 'MVB2EDF23FRSFS',
                  use: 1,
                  required: 0,
                  model: 2001,
                  licensePlate: '21GFH',
                  inciso: 16,
                  brand: 2489,
                  client2: 'Ricardo Esparza',
                  beneficiary: 'Ricardo Esparza',
                },
              ],
            },
          },
        }

      case SEARCH_EXCEL:
        return {}

      case SEARCH_MAIL_TO:
        return {}
    }
  },

  buildUrlBase: (path, params, method) => {
    return `/api${path}?${params}${method}`
  },
}))

describe('Search page', () => {
  test('test', async () => {
    global.window = Object.create(window)
    Object.defineProperty(window, 'location', {
      value: {
        search: {
          search: '1GNCS4564165M5',
        },
      },
    })

    await act(async () => {
      wrapper = getWrapper(Search)
    })
    wrapper.update()

    wrapper.find('Button').find('#download').first().simulate('click')
    // wrapper.find('Button').find('#email').first().simulate('click')

    // test sort
    for (let i = 0; i < 4; i++) {
      wrapper
        .find('Table')
        .at(1)
        .find('.ant-table-thead tr')
        .childAt(i)
        .simulate('click')
    }

    // test sort
    for (let i = 0; i < 10; i++) {
      wrapper
        .find('Table')
        .at(2)
        .find('.ant-table-thead tr')
        .childAt(i)
        .simulate('click')
    }

    expect(wrapper).not.toBeNull()
  })
})

describe('Search page not found', () => {
  it('test', () => {
    wrapper = getWrapper(Search)

    expect(wrapper).not.toBeNull()
  })
})
