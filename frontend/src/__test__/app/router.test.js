import React from 'react'
import { configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducers from '@reducers'
import { BrowserRouter as RRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import Cookies from 'js-cookie'
import { IntlProvider } from 'react-intl'
import es from '@utils/translations/es.json'
import Router from '../../app/router'

configure({ adapter: new Adapter() })

const getWrapper = Component =>
  mount(
    <Provider
      store={createStore(reducers, {}, applyMiddleware(thunkMiddleware))}
    >
      <IntlProvider locale="es" messages={es}>
        <RRouter>
          <Component />
        </RRouter>
      </IntlProvider>
    </Provider>
  )

describe('App Router', () => {
  it('test', () => {
    const wrapper = getWrapper(Router)
    expect(wrapper).not.toBeNull()
  })
})

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
})

describe('App Router login', () => {
  it('test', () => {
    Cookies.set(
      'token',
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwidHlwZV9ncm91cCI6ImFkbWluIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.YcAZZ5Nq1T_38jZBQExI2TxlYp48pPM6wPt8xOR6HD0'
    )
    const wrapper = getWrapper(Router)
    expect(wrapper).not.toBeNull()
  })
})
