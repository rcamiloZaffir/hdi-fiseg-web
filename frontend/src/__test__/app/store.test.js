import React from 'react'
import { configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { Provider } from 'react-redux'
import initializeStore from '../../app/store'

configure({ adapter: new Adapter() })

const store = initializeStore()

const getWrapper = () => mount(<Provider store={store}>My Store</Provider>)

describe('App Store', () => {
  it('test', () => {
    const wrapper = getWrapper()
    expect(wrapper).not.toBeNull()
  })
})
