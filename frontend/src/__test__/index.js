import React from 'react'
import { configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducers from '@reducers'
import { Provider } from 'react-redux'
import { IntlProvider } from 'react-intl'
import es from '@utils/translations/es.json'

configure({ adapter: new Adapter() })

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
})

HTMLCanvasElement.prototype.getContext = () => {
  // return whatever getContext has to return
}

export const getWrapper = Component =>
  mount(
    <Provider
      store={createStore(reducers, {}, applyMiddleware(thunkMiddleware))}
    >
      <IntlProvider locale="es" messages={es}>
        <Component />
      </IntlProvider>
    </Provider>
  )
