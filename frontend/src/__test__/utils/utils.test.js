import { buildUrlBase, ClientHttpRequest } from '@utils/httpService'
import { GET, POST, PUT, DELETE, PATCH } from '@utils/constants'
import {
  capitalize,
  urlParams,
  number,
  number2,
  dateformat,
  dateformat2,
  isAuthenticated,
  parseJoyErrors,
  downloadBlob,
} from '@utils'
import uris from '@utils/uris'

jest.mock('axios', () => ({
  get: async (url, settings) => {
    return {
      data: { url, settings },
    }
  },
  post: async (url, settings) => {
    return {
      data: { url, settings },
    }
  },
  put: async (url, settings) => {
    return {
      data: { url, settings },
    }
  },
  delete: async (url, settings) => {
    return {
      data: { url, settings },
    }
  },
  patch: async (url, settings) => {
    return {
      data: { url, settings },
    }
  },
}))

describe('utils', () => {
  /**
   * httpServices
   */

  it('should return buildUrlBase', () => {
    const c = buildUrlBase('base', 'params', 'method')
    expect(c).not.toBeNull()
  })

  it('should return ClientHttpRequest', async () => {
    let data = await ClientHttpRequest({
      method: GET,
      path: '',
      params: {},
    })
    expect(data).not.toBeNull()

    data = await ClientHttpRequest({
      method: POST,
      path: '',
      params: {},
    })
    expect(data).not.toBeNull()

    data = await ClientHttpRequest({
      method: PUT,
      path: '',
      params: {},
    })
    expect(data).not.toBeNull()

    data = await ClientHttpRequest({
      method: DELETE,
      path: '',
      params: {},
    })
    expect(data).not.toBeNull()

    data = await ClientHttpRequest({
      method: PATCH,
      path: '',
      params: {},
    })
    expect(data).not.toBeNull()
  })

  /**
   * index
   */

  it('should return capitalize', () => {
    const c = capitalize('hi')
    expect(c).toEqual('Hi')
  })

  it('should return capitalize null', () => {
    const c = capitalize(3)
    expect(c).toEqual('')
  })

  it('should return utils', () => {
    expect(urlParams('str')).toBeNull()
    expect(number(20000)).toEqual('20,000')
    expect(number2(200000)).toEqual('200 k')
    expect(dateformat(new Date())).not.toBeNull()
    expect(dateformat2(new Date())).not.toBeNull()
    expect(isAuthenticated()).toBeFalsy()

    // Cookies.set(
    //   'token',
    //   'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwidHlwZV9ncm91cCI6ImFkbWluIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.YcAZZ5Nq1T_38jZBQExI2TxlYp48pPM6wPt8xOR6HD0'
    // )
    // expect(isAuthenticated()).toEqual(false)

    expect(parseJoyErrors([])).not.toBeNull()
  })

  /**
   * uris
   */

  it('should return uris', () => {
    expect(uris).not.toBeNull()
  })

  global.URL.createObjectURL = jest.fn()

  it('should return downloadBlob', () => {
    expect(
      downloadBlob(new Blob(['<html>…</html>'], { type: 'text/html' }), '')
    ).not.toBeNull()
  })
})
