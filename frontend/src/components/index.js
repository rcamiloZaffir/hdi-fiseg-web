import IconText from './IconText'
import GraphMonth from './GraphMonth'
import GraphYear from './GraphYear'
import FooterCar from './FooterCar'
import IconBack from './IconBack'
import Table from './Table'
import Slider from './Slider'

export { IconText, GraphMonth, GraphYear, FooterCar, IconBack, Table, Slider }
