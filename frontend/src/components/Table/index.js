/**
 * Componente web para generalizar el Table
 * @module components/GraphMonth
 */
import React from 'react'
import { Table } from 'antd'
import { number } from '@utils'

/**
 * Constructor
 * @param {JSON} params onfiguracion del componente web
 */
const Index = ({ columns, dataSource, pagination, onChange }) => {
  return (
    <Table
      dataSource={
        dataSource
          ? dataSource.map((data, key) => ({ ...data, ...{ key } }))
          : []
      }
      columns={columns}
      pagination={{
        ...pagination,
        ...{
          showSizeChanger: 1,
          defaultPageSize: 15,
          pageSizeOptions: [15, 30, 60, 90],
          showTotal: total => `Total : ${number(total)} `,
        },
      }}
      scroll={{ x: 900, y: 300 }}
      onChange={onChange}
    />
  )
}

export default Index
