/**
 * Componente web de graficas del mes
 * @module components/GraphMonth
 */
import React, { useState } from 'react'
import { Row, Col, Divider, Button, Select, Tooltip, DatePicker } from 'antd'
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  VerticalBarSeries,
  DiscreteColorLegend,
  Hint,
} from 'react-vis'
import moment from 'moment'
import { PieChart, d3 } from 'react-d3-components'
import AssignmentReturnedIcon from '@material-ui/icons/AssignmentReturned'
import MailOutlineIcon from '@material-ui/icons/MailOutline'
import { COLOR } from '@utils/constants'
import { number, number2, capitalize } from '@utils'

import { IconText } from '@components'

const mailActive = process.env.REACT_APP_SEND_MAIL_ACTIVE_FE

/**
 * Constructor
 * @param {JSON} params Parametros del Componente Web
 */
const GraphMonth = ({
  className,
  name,
  data,
  form,
  onExportPdf,
  onSendEmail,
  onRefresh,
  onChange,
}) => {
  const {
    total,
    individual,
    flotilla,
    graph: { individualResidente, individualTurista, flotillaResidente },
  } = data

  const graph100 = individualResidente + individualTurista + flotillaResidente
  const percent = [
    ((individualResidente * 100) / graph100).toFixed(2),
    ((individualTurista * 100) / graph100).toFixed(2),
    ((flotillaResidente * 100) / graph100).toFixed(2),
  ]
  const [value, setValue] = useState()
  const topltip = val => {
    setValue({
      x: val.x,
      y: number(val.y),
    })
  }
  const [dates, setDates] = useState([])
  const disabledDate = current => {
    if (!dates || dates.length === 0) {
      return current > moment().endOf('year')
    }
  }

  return (
    <div className={className} id={name}>
      <h2>Resumen del Mes Anterior</h2>

      <Row>
        <Col lg={16} md={24}>
          <div style={{ display: 'inline-flex' }}>
            <div className="text-bottom" style={{ width: '60px' }}>
              <span>
                <b>Total</b>
                <br /> mensual
              </span>
            </div>

            <Tooltip title={number(total)}>
              <span className="f40">{number2(total)}</span>
            </Tooltip>

            <div className="ml-2">
              <h4>Periodo</h4>
              <div style={{ display: 'inline-flex' }}>
                <Select
                  id="mounth"
                  className="mr-2"
                  defaultValue={
                    form.month === -1
                      ? moment().month()
                      : parseInt(moment().subtract(2, 'month').format('MM'), 10)
                  }
                  style={{ width: 120 }}
                  onChange={(_value, key) => onChange((_value, key), 'month')}
                >
                  {moment.months().map((k, v) => (
                    <Select.Option key={k} value={v}>
                      {capitalize(k)}
                    </Select.Option>
                  ))}
                </Select>

                <DatePicker
                  id="year"
                  format="YYYY"
                  allowClear={false}
                  value={moment(form.year, 'YYYY')}
                  className="mr-2"
                  disabledDate={disabledDate}
                  onChange={(_value, key) => onChange({ value: key }, 'year')}
                  onCalendarChange={val => {
                    setDates(val)
                  }}
                  picker="year"
                />
                <Button
                  id="ver"
                  className="btn-gray"
                  style={{ width: 100 }}
                  onClick={onRefresh}
                >
                  Ver
                </Button>
              </div>
            </div>
          </div>
        </Col>

        <Col
          lg={8}
          md={24}
          style={{ display: 'flex', justifyContent: 'flex-end' }}
        >
          <div style={{ display: 'inline-flex' }}>
            <IconText
              className="mr-3"
              img="car2"
              data={flotilla}
              title="Flotilla"
            />
            <IconText
              className="mr-2"
              img="car"
              data={individual}
              title="Individual R"
            />
            <Button
              id="download"
              type="secondary"
              shape="circle"
              className="btn-circle"
              icon={<AssignmentReturnedIcon />}
              onClick={() => onExportPdf(name)}
            />
            {mailActive === 'true' && (
              <Button
                id="email"
                type="secondary"
                shape="circle"
                className="btn-circle"
                icon={<MailOutlineIcon />}
                onClick={() => onSendEmail(name)}
              />
            )}
          </div>
        </Col>
      </Row>

      <Divider />

      <Row>
        <Col lg={12} md={24}>
          <Row>
            <Col>
              <XYPlot
                xType="ordinal"
                width={400}
                height={250}
                stackBy="y"
                margin={{ left: 60, right: 10, top: 10, bottom: 40 }}
              >
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis />
                <YAxis tickFormat={d => number2(d)} />

                <VerticalBarSeries
                  onValueMouseOver={topltip}
                  onValueMouseOut={() => setValue()}
                  data={[
                    { x: `Individual R`, y: individualResidente },
                    { x: 'Individual T', y: 0 },
                    { x: 'Flotilla', y: 0 },
                  ]}
                  color={COLOR.primary}
                />

                <VerticalBarSeries
                  onValueMouseOver={topltip}
                  onValueMouseOut={() => setValue()}
                  data={[
                    { x: 'Individual R', y: 0 },
                    { x: 'Individual T', y: individualTurista },
                    { x: 'Flotilla', y: 0 },
                  ]}
                  color={COLOR.gray}
                />

                <VerticalBarSeries
                  onValueMouseOver={topltip}
                  onValueMouseOut={() => setValue()}
                  data={[
                    { x: 'Individual R', y: 0 },
                    { x: 'Individual T', y: 0 },
                    { x: 'Flotilla', y: flotillaResidente },
                  ]}
                  color={COLOR.secondary}
                />
                {value ? <Hint value={value} /> : null}
              </XYPlot>
            </Col>
            <Col>
              <DiscreteColorLegend
                items={[
                  `Individual R ${percent[0]}%`,
                  `Invidivual T ${percent[1]}%`,
                  `Flotilla R ${percent[2]}%`,
                ]}
                colors={[COLOR.primary, COLOR.gray, COLOR.secondary]}
              />
            </Col>
          </Row>
        </Col>
        <Col span={12}>
          <Row>
            <Col>
              <PieChart
                data={
                  total > 0
                    ? {
                        values: [
                          { x: `Individual R`, y: percent[0] },
                          { x: `Invidivual T`, y: percent[1] },
                          { x: `Flotilla F`, y: percent[2] },
                        ],
                      }
                    : {
                        values: [{ x: `0%`, y: 1 }],
                      }
                }
                width={350}
                height={200}
                colorScale={d3.scale
                  .ordinal()
                  .range([COLOR.primary, COLOR.gray, COLOR.secondary])}
              />
            </Col>
            <Col>
              <DiscreteColorLegend
                items={[
                  `Individual R ${percent[0]}%`,
                  `Invidivual T ${percent[1]}%`,
                  `Flotilla R ${percent[2]}%`,
                ]}
                colors={[COLOR.primary, COLOR.gray, COLOR.secondary]}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  )
}

export default GraphMonth
