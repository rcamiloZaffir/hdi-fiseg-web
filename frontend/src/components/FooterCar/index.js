/**
 * Componente web de FooterCar
 * @module components/FooterCar
 */
import React from 'react'

/**
 * Constructor
 */
const FooterCar = () => (
  <footer className="card-footer pt-4">
    <img src="/img/car.svg" alt="car" />
  </footer>
)

export default FooterCar
