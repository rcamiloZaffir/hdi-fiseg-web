/**
 * Componente web de retorno de pagina web
 * @module components/IconBack
 */
import React from 'react'
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace'

/**
 * Constructor
 * @param {String} params Link de retorno de pagina web
 */
const IconBack = ({ to }) => (
  <a href={to}>
    <div style={{ float: 'right' }}>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <KeyboardBackspaceIcon />
        <span className="ml-1">Regresar</span>
      </div>
    </div>
  </a>
)

export default IconBack
