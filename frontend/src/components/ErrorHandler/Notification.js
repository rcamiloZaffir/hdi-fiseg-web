/**
 * Manejo del flujo de notificaciones
 * @module components/Notification
 */
import { notification } from 'antd'
import { closeNotification } from '@actions/errorHandler'

/**
 * Constructor de Notificaciones
 * @param {JSON} config Configuraciones de NOtificaciones
 * @param {Object} dispatch Objeto para executar acciones de Redux
 */
/* istanbul ignore next */
const Notification = (config, dispatch) => {
  const { level, route } = config
  switch (level) {
    case 'success': {
      notification.success({
        message: config.title,
        description: config.message,
        // duration: 3,
        onClose: () => dispatch(closeNotification(route)),
      })
      break
    }
    case 'warning': {
      notification.warning({
        message: config.title,
        description: config.message,
        // duration: 3,
        onClose: () => dispatch(closeNotification(route)),
      })
      break
    }
    case 'error': {
      notification.error({
        message: config.title,
        description: config.message,
        // duration: 3,
        onClose: () => dispatch(closeNotification(route)),
      })
      break
    }
    case 'info': {
      notification.info({
        message: config.title,
        description: config.message,
        // duration: 3,
        onClose: () => dispatch(closeNotification(route)),
      })
      break
    }
  }
}

export default Notification
