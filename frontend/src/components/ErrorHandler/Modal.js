/**
 * Manejo de estados del Modal
 * @module components/Modal
 */
import React from 'react'
import { Modal as ModalAntd } from 'antd'
import { closeNotification } from '@actions/errorHandler'

/**
 * Constructor del Modal
 * @param {JSON} config COnfiguraciones del Modal
 * @param {Object} dispatch  Objecto para ejecutar acciones de Redux
 */
/* istanbul ignore next */
const Modal = (config, dispatch) => {
  const { level, route } = config

  switch (level) {
    case 'success': {
      ModalAntd.success({
        title: config.title,
        content: (
          <div>
            <p>{config.message}</p>
          </div>
        ),
        onOk() {
          dispatch(closeNotification(route))
        },
      })
      break
    }
    case 'warning': {
      ModalAntd.warning({
        title: config.title,
        content: config.message,
        onOk() {
          dispatch(closeNotification(route))
        },
      })
      break
    }
    case 'error': {
      ModalAntd.error({
        title: config.title,
        content: (
          <div>
            <p>{config.message}</p>
          </div>
        ),
        onOk() {
          dispatch(closeNotification(route))
        },
      })
      break
    }
    case 'info': {
      ModalAntd.info({
        title: config.title,
        content: (
          <div>
            <p>{config.message}</p>
          </div>
        ),
        onOk() {
          dispatch(closeNotification(route))
        },
      })
      break
    }
    // No default
  }
}

export default Modal
