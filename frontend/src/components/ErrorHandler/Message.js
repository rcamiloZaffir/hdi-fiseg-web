/**
 * Manejo de Mensajes
 * @module components/Message
 */
import { message } from 'antd'
import { closeNotification } from '@actions/errorHandler'

/**
 * Constructor
 * @param {JSON} config JSon de configuraciones
 * @param {Object} dispatch Objecto para ejectar acciones de Redux
 */
/* istanbul ignore next */
const Message = (config, dispatch) => {
  const { level } = config
  switch (level) {
    case 'success': {
      message.success({
        content: config.message,
        onClose: () => dispatch(closeNotification()),
      })
      break
    }
    case 'warning': {
      message.warning({
        content: config.message,
        onClose: () => dispatch(closeNotification()),
      })
      break
    }
    case 'error': {
      message.error({
        content: config.message,
        onClose: () => dispatch(closeNotification()),
      })
      break
    }
    case 'info': {
      message.info({
        content: config.message,
        onClose: () => dispatch(closeNotification()),
      })
      break
    }
    // No default
  }
}
export default Message
