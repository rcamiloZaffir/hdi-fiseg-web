/**
 * Modulo de Manejo de errores
 * @module components
 */
import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { reqLogin } from '@actions/login'
import Message from './Message'
import Modal from './Modal'
import Notification from './Notification'

/**
 * COnstructor 2
 * @param {JSON} config JSon de configuraciones
 * @param {Object} dispatch Objeto para ejecutar actions de redux
 */
/* istanbul ignore next */
const ManageErrors = (config, dispatch) => {
  const { type, statusCode } = config
  if (statusCode === 408) {
    dispatch(reqLogin())
  } else if (statusCode !== 408) {
    switch (type) {
      case 'modal': {
        Modal(config, dispatch)
        break
      }
      case 'message': {
        Message(config, dispatch)
        break
      }
      case 'notification': {
        Notification(config, dispatch)
        break
      }
      default:
        return null
    }
  } else return null
}

const Index = props => {
  const { errorHandler } = props
  const [__error, setError] = useState(errorHandler)
  const dispatch = useDispatch()

  useEffect(() => {
    const { open: openedState } = __error
    const { open: openedProps } = errorHandler

    if (openedProps !== openedState) {
      setError(errorHandler)
      ManageErrors(errorHandler, dispatch)
    }
  })
  return (
    <div>
      {/* Here there is nothing cause antd launch messages, modals, notifications, calling a functions */}
    </div>
  )
}

export default Index
