/**
 * Componente web de icon con texto
 * @module components/IconText
 */
import React from 'react'
import { Row, Col, Avatar, Tooltip } from 'antd'
import { number, number2 } from '@utils'

/**
 * Constructor
 * @param {JSON} params Configuracion del componente web
 */
const Index = ({ className, icon, img, data, title }) => {
  function getImg(name) {
    switch (name) {
      case 'car2':
        return <img src="img/car-group.svg" alt="" />
      case 'car':
        return <img src="img/car-single.svg" alt="" />
      case 'extraidos':
        return <img src="img/iconExtraidos.svg" alt="" className="my-white" />
      case 'procesados':
        return <img src="img/iconProcesados.svg" alt="" className="my-white" />
      case 'exitosos':
        return <img src="img/iconExitosos.svg" alt="" className="my-white" />
      case 'reintentos':
        return <img src="img/iconReitentos.svg" alt="" className="my-white" />
      case 'conError':
        return <img src="img/iconConError.svg" alt="" className="my-white" />
    }
  }

  return (
    <div className={className}>
      <Tooltip title={number(data)}>
        <Row>
          <Col style={{ display: 'flex' }}>
            {img && <Avatar shape="square" size={24} icon={getImg(img)} />}
            {icon && (
              <Avatar size={24} className="icon-text-primary" icon={icon} />
            )}
            <h2 className="mb-0 ml-1">{number2(data)}</h2>
          </Col>
        </Row>
        <h6>{title}</h6>
      </Tooltip>
    </div>
  )
}

export default Index
