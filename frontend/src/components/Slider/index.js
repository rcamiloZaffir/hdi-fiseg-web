/**
 * Componente web para mopstrar los status
 * @module components/Slider
 */
import React from 'react'
import { Slider as SliderAntd } from 'antd'
import { HDI_STATUS, HDI_FILTERS } from '@utils/constants'
import { COLOR } from '../../utils/constants'

/**
 * COnstructor
 * @param {JSON} params de componente web
 */
const Slider = ({ statusName }) => {
  const marks = {
    0: {
      style: {
        paddingTop: '20px',
      },
      label: <strong>Extraído</strong>,
    },
    50: {
      style: {
        paddingTop: '20px',
      },
      label: <strong>Procesado</strong>,
    },
    100: {
      style: {
        paddingTop: '20px',
      },
      label: <strong>Exitoso</strong>,
    },
  }

  let status = ''
  switch (statusName) {
    case HDI_FILTERS[0].text:
      status = HDI_STATUS.extract
      break
    case HDI_FILTERS[1].text:
      status = HDI_STATUS.process
      break
    case HDI_FILTERS[2].text:
    case HDI_FILTERS[3].text:
      status = HDI_STATUS.success
      break
    case HDI_FILTERS[4].text:
    case HDI_FILTERS[5].text:
    case HDI_FILTERS[6].text:
      status = HDI_STATUS.error
      break
  }

  if (status === HDI_STATUS.error)
    marks['100'] = {
      style: {
        paddingTop: '20px',
        color: COLOR.error,
      },
      label: <strong>Error</strong>,
    }

  let value
  let cssError = ''
  switch (status) {
    case HDI_STATUS.extract:
      value = 0
      break
    case HDI_STATUS.process:
      value = 50
      break
    case HDI_STATUS.success:
      value = 100
      break
    case HDI_STATUS.error:
      value = 100
      cssError = 'slider-car-error'
      break
  }

  return (
    <SliderAntd
      disabled
      className={`slider-car  ${cssError}`}
      tooltipVisible={false}
      marks={marks}
      defaultValue={value}
    />
  )
}

export default Slider
