/**
 * Componente web para el status de los automoviles
 * @module components/GraphMonth
 */
import React from 'react'
import { useSelector } from 'react-redux'
import { Spin } from 'antd'

/**
 * Constructor
 */
const Spinner = () => {
  const loading = useSelector(state => state.spinner.loading)

  if (loading) {
    return (
      <div className="overlay-iw">
        <Spin size="large" tip="CARGANDO..." />
      </div>
    )
  }
  return null
}

export default Spinner
