/**
 * Componente web de graficas del ano
 * @module components/GraphYear
 */
import React, { useState } from 'react'
import { Row, Col, Divider, Button, Tooltip } from 'antd'
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  HorizontalBarSeries,
  VerticalGridLines,
  DiscreteColorLegend,
  Hint,
} from 'react-vis'
import AssignmentReturnedIcon from '@material-ui/icons/AssignmentReturned'
import MailOutlineIcon from '@material-ui/icons/MailOutline'
import moment from 'moment'
import { COLOR } from '@utils/constants'
import { number, number2 } from '@utils'
import { IconText } from '@components'

const mailActive = process.env.REACT_APP_SEND_MAIL_ACTIVE_FE

/**
 * Constructor
 * @param {JSON} params parametros del componente web
 */
const GraphYear = ({
  className,
  name,
  data,
  onExportPdf,
  onSendEmail,
  // onRefresh,
}) => {
  const today = moment().format('DD/MM/YYYY')

  if (data && data.totalesEstatus) {
    const {
      total,
      totalesEstatus: {
        porcentajeError,
        extraidos,
        procesados,
        exitosos,
        reintentos,
        error,
      },
      months,
    } = data
    const [value, setValue] = useState()

    const topltip = val => {
      setValue({
        y: val.y,
        x: number(val.x),
      })
    }

    return (
      <div className={className} id={name}>
        <h2>Movimientos al {today}</h2>

        <Row>
          <Col lg={12} md={24}>
            <div style={{ display: 'inline-flex' }}>
              <div className="mr-2">
                <br />
                Total de <br />
                Movimientos
              </div>

              <div>
                <Tooltip title={number(total)}>
                  <span className="f40">{number2(total)}</span>
                </Tooltip>
              </div>

              <div className="ml-2">
                <h4 className="text-center">Errores registrados</h4>
                <div style={{ display: 'inline-flex' }}>
                  <h3 className="mr-2">{porcentajeError.toFixed(2)}%</h3>
                  <h6>
                    Promedio <br /> por carga
                  </h6>
                </div>
              </div>
            </div>
          </Col>
          <Col
            lg={12}
            md={24}
            style={{ display: 'flex', justifyContent: 'flex-end' }}
          >
            <div style={{ display: 'inline-flex' }}>
              <IconText
                className="mr-2"
                img="extraidos"
                data={extraidos}
                title="Extraídos"
              />
              <IconText
                className="mr-2"
                img="procesados"
                data={procesados}
                title="Procesados"
              />
              <IconText
                className="mr-2"
                img="exitosos"
                data={exitosos}
                title="Exitosos"
              />
              <IconText
                className="mr-2"
                img="reintentos"
                data={reintentos}
                title="Reintentos"
              />
              <IconText
                className="mr-2"
                img="conError"
                data={error}
                title="Con error"
              />
              <Button
                id="download"
                type="secondary"
                shape="circle"
                className="btn-circle"
                onClick={() => onExportPdf(name)}
                icon={<AssignmentReturnedIcon />}
              />
              {mailActive === 'true' && (
                <Button
                  id="email"
                  type="secondary"
                  shape="circle"
                  className="btn-circle"
                  onClick={() => onSendEmail(name)}
                  icon={<MailOutlineIcon />}
                />
              )}
            </div>
          </Col>
        </Row>

        <Divider />

        <Row>
          <Col lg={20} md={24} style={{ overflow: 'auto' }}>
            {months.length === 4 && (
              <XYPlot yType="ordinal" width={800} height={300} className="mb-1">
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis tickLabelAngle={-45} tickFormat={v => number2(v)} />
                <YAxis left={2} />

                <HorizontalBarSeries
                  onValueMouseOver={topltip}
                  onValueMouseOut={() => setValue()}
                  data={[
                    {
                      y: moment(months[0].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[0].individualResidente,
                    },
                    {
                      y: moment(months[1].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[1].individualResidente,
                    },
                    {
                      y: moment(months[2].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[2].individualResidente,
                    },
                    {
                      y: moment(months[3].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[3].individualResidente,
                    },
                  ]}
                  color={COLOR.primary}
                />

                <HorizontalBarSeries
                  onValueMouseOver={topltip}
                  onValueMouseOut={() => setValue()}
                  data={[
                    {
                      y: moment(months[0].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[0].individualTuristas,
                    },
                    {
                      y: moment(months[1].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[1].individualTuristas,
                    },
                    {
                      y: moment(months[2].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[2].individualTuristas,
                    },
                    {
                      y: moment(months[3].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[3].individualTuristas,
                    },
                  ]}
                  color={COLOR.gray}
                />
                <HorizontalBarSeries
                  onValueMouseOver={topltip}
                  onValueMouseOut={() => setValue()}
                  data={[
                    {
                      y: moment(months[0].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[0].flotillaResidente,
                    },
                    {
                      y: moment(months[1].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[1].flotillaResidente,
                    },
                    {
                      y: moment(months[2].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[2].flotillaResidente,
                    },
                    {
                      y: moment(months[3].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[3].flotillaResidente,
                    },
                  ]}
                  color={COLOR.secondary}
                />
                <HorizontalBarSeries
                  onValueMouseOver={topltip}
                  onValueMouseOut={() => setValue()}
                  data={[
                    {
                      y: moment(months[0].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[0].error,
                    },
                    {
                      y: moment(months[1].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[1].error,
                    },
                    {
                      y: moment(months[2].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[2].error,
                    },
                    {
                      y: moment(months[3].monthYear, 'YYYYMM')
                        .format('MMM')
                        .toUpperCase(),
                      x: months[3].error,
                    },
                  ]}
                  color={COLOR.error}
                />

                {value ? <Hint value={value} /> : null}
              </XYPlot>
            )}
          </Col>

          <Col lg={4} md={24}>
            <DiscreteColorLegend
              items={['Individual R', 'Individual T', 'Flotilla', 'Error']}
              colors={[COLOR.primary, COLOR.gray, COLOR.secondary, COLOR.error]}
            />
          </Col>
        </Row>
      </div>
    )
  }
  return <></>
}

export default GraphYear
