/**
 * Pagina de login
 * @module pages/login
 */
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useSpring, animated } from 'react-spring'
import { Button, Form, Input, Row, Col, Card } from 'antd'
import { FormattedMessage, useIntl } from 'react-intl'
import FaceIcon from '@material-ui/icons/Face'
import LockIcon from '@material-ui/icons/Lock'
import { login } from '@actions/login'
import { FADE_TRANSITION } from '@utils/constants'
import { parseJoyErrors } from '@utils'
import { formInit, formChange, formValidate } from '@actions/form'
import schema from './schema'
// import Cookies from 'js-cookie'

/**
 * @cosntructor
 */
const Login = () => {
  const dispatch = useDispatch()
  const fade = useSpring(FADE_TRANSITION)
  const { form } = useSelector(state => state)
  const { formatMessage } = useIntl()

  useEffect(() => {
    // form
    dispatch(
      formInit({
        username: '',
        password: '',
      })
    )
  }, [])

  const onChangeItemValue = e => {
    const { name, value } = e.target
    dispatch(formChange(name, value))
  }

  const validateSchema = () => {
    const { error } = schema.validate(form.form, { abortEarly: false })
    if (error) {
      dispatch(formValidate(parseJoyErrors(error.details)))
    } else {
      dispatch(login(form.form))
    }
  }

  const handleKeypress = e => {
    if (e.key === 'Enter') {
      validateSchema()
    }
  }

  return (
    <animated.div style={fade}>
      <div className="login">
        <img className="bg" src="/img/bg-login.png" alt="bg-login" />
        <img className="logo" src="/img/logo.png" alt="bg-login" />

        <Form className="title">
          <Row className="ant-row-center">
            <Col>
              <p>
                <FormattedMessage id="login.title" />
              </p>
            </Col>
          </Row>
        </Form>

        <div className="center-iw">
          <Card style={{ width: 500 }}>
            <Form className="login-form">
              <Row className="ant-row-center">
                <Col>
                  <h1>Iniciar Sesión</h1>
                </Col>
              </Row>

              <Row className="ant-row-center">
                <Col>
                  <Form.Item
                    label={
                      <FaceIcon
                        className="user-icon"
                        style={{ color: '#006729' }}
                      />
                    }
                    hasFeedback={form.hasFeedback}
                    validateStatus={
                      form.validate.username ? 'error' : 'success'
                    }
                    help={
                      form.validate.username && form.validate.username.message
                    }
                  >
                    <Input
                      placeholder={formatMessage({
                        id: 'login.placeholder.username',
                      })}
                      style={{ width: '300px' }}
                      id="username"
                      name="username"
                      value={form.form.username}
                      onChange={onChangeItemValue}
                      onKeyPress={handleKeypress}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Row className="ant-row-center">
                <Col>
                  <Form.Item
                    label={
                      <LockIcon
                        className="user-icon"
                        style={{ color: '#006729' }}
                      />
                    }
                    hasFeedback={form.hasFeedback}
                    validateStatus={
                      form.validate.password ? 'error' : 'success'
                    }
                    help={
                      form.validate.password && form.validate.password.message
                    }
                  >
                    <Input.Password
                      placeholder={formatMessage({
                        id: 'login.placeholder.password',
                      })}
                      style={{ width: '300px' }}
                      id="password"
                      name="password"
                      value={form.form.password}
                      onChange={onChangeItemValue}
                      onKeyPress={handleKeypress}
                    />
                  </Form.Item>
                </Col>
              </Row>

              <Row className="ant-row-center">
                <Col span={6}>
                  <Form.Item>
                    <Button
                      block
                      type="primary"
                      onClick={() => validateSchema()}
                    >
                      <FormattedMessage id="login.button.send" />
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form>

            {/* <Form className="login-form">
              <Row className="ant-row-center">
                <Col>
                  <h1>Iniciar Sesión</h1>
                </Col>
              </Row>

              <Row className="ant-row-center">
                <Col span={6}>
                  <Form.Item>
                    <Button
                      block
                      type="primary"
                      onClick={() => {
                        Cookies.set('token', 'token')
                        Cookies.set('username', 'username')
                        Cookies.set('email', 'email')
                        window.location.reload()
                      }}
                    >
                      <FormattedMessage id="login.button.send" />
                    </Button>
                  </Form.Item>
                </Col>
              </Row>
            </Form> */}
          </Card>
        </div>
        <div className="center-iw" style={{ marginTop: '20px' }}>
          <img src="/img/car.svg" alt="car" style={{ width: '9em' }} />
        </div>
      </div>
    </animated.div>
  )
}

export default Login
