import Joi from '@hapi/joi'
import { MESSAGE_VALIDATIONS } from '@utils/constants'

export default Joi.object({
  username: Joi.string().required().messages(MESSAGE_VALIDATIONS),

  password: Joi.string().required().messages(MESSAGE_VALIDATIONS),
})
