/**
 * Pagina  para mostrar los avances por media de graficas al mes y ano
 * @module pages/Dashboard
 */
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Card, notification } from 'antd'
import { useSpring, animated } from 'react-spring'
import html2pdf from 'html2pdf.js'
import moment from 'moment'
import { GraphMonth, GraphYear, FooterCar, Table } from '@components'
import { FADE_TRANSITION, DATE_DDMMYYYY, pdfOptions } from '@utils/constants'
import { number, dateformatHour, capitalize, isAuthenticated } from '@utils'
import { httpGet, httpPost, httpInit } from '@actions/http'
import { formInit, formChange } from '@actions/form'
import {
  DASHBOARD_GRAPH,
  DASHBOARD_GRAPH_HISTORY,
  DASHBOARD_GRAPH_PROCESSING_HISTORY,
  MAIL_TO,
} from '@utils/services'

/**
 * @cosntructor
 */
const Dashboard = () => {
  const fade = useSpring(FADE_TRANSITION)
  const dispatch = useDispatch()
  const { http, form } = useSelector(state => state)
  const [currentDate, setCurrentDate] = useState(moment().format(DATE_DDMMYYYY))

  const datasource =
    http[DASHBOARD_GRAPH_PROCESSING_HISTORY] &&
    http[DASHBOARD_GRAPH_PROCESSING_HISTORY].historia
      ? http[DASHBOARD_GRAPH_PROCESSING_HISTORY].historia
      : false

  const [sortOrder, setSortOrder] = useState({})
  const calculatedYear =
    moment().month() === 0
      ? moment().subtract(1, 'year').format('YYYY')
      : moment().year()

  const calculatedMonth =
    form.month === -1
      ? moment().month()
      : parseInt(moment().subtract(2, 'month').format('MM'), 10)

  useEffect(() => {
    notification.destroy()

    // form
    dispatch(
      formInit({
        month: calculatedMonth,
        year: calculatedYear,
      })
    )

    // services
    dispatch(httpInit())
    dispatch(
      httpGet(
        DASHBOARD_GRAPH,
        {
          month: moment().month(),
          year: moment().year(),
        },
        data => {
          notification.info({
            message: `${
              data.total || 0
            } movimientos se han enviado para su registro: para su seguimiento consulte la sección Movimientos AMIS ...`,
            duration: 5,
          })
        }
      )
    )
    dispatch(httpGet(DASHBOARD_GRAPH_HISTORY))
    dispatch(httpGet(DASHBOARD_GRAPH_PROCESSING_HISTORY))
  }, [])

  function savePdf(id, name) {
    pdfOptions.filename = `${name}.pdf`
    html2pdf().from(document.getElementById(id)).set(pdfOptions).save()
  }

  async function savePdftoString(id) {
    return html2pdf()
      .from(document.getElementById(id))
      .set(pdfOptions)
      .outputPdf()
  }

  const getPdfTitle = name => {
    switch (name) {
      case 'GraphMonth':
        return 'Resumen del mes anterior'
      case 'GraphYear':
        return 'Resumen de los 4 meses anteriores'
    }
  }

  const onExportPdf = name => {
    savePdf(name, getPdfTitle(name))

    notification.info({
      message: `Los movimientos de registros con los filtros selecionados, se descargaron en su equipo.`,
      duration: 5,
    })
  }

  const onSendEmail = async name => {
    const pdf = await savePdftoString(name)
    const pdfBase64 = btoa(pdf)

    dispatch(
      httpPost(
        MAIL_TO,
        {
          title: getPdfTitle(name),
          email: isAuthenticated().email,
          file: {
            type: 'application/pdf',
            data: pdfBase64,
          },
        },
        () => {
          notification.info({
            message: 'Email enviado correctamente',
            description: `Se envió el archivo pdf "${getPdfTitle(name)}"`,
            duration: 10,
          })
        }
      )
    )
  }

  const renderDate = date => <span>{dateformatHour(date)}</span>

  const onChange = (page, filters, sorter) => {
    setSortOrder(sorter)
  }

  return (
    <animated.div style={fade}>
      <Card>
        <h1 className="mb-0">Bienvenido</h1>
        <h6>
          {http[DASHBOARD_GRAPH] ? number(http[DASHBOARD_GRAPH].total) : 0}{' '}
          pólizas registradas : {currentDate}
        </h6>

        <GraphMonth
          className="mt-4"
          name="GraphMonth"
          form={form.form}
          data={
            http[DASHBOARD_GRAPH]
              ? http[DASHBOARD_GRAPH]
              : {
                  total: 0,
                  individual: 0,
                  flotilla: 0,
                  graph: {
                    individualResidente: 0,
                    individualTurista: 0,
                    flotillaResidente: 0,
                  },
                }
          }
          onExportPdf={onExportPdf}
          onSendEmail={onSendEmail}
          onRefresh={() => {
            notification.destroy()

            const currentMonth = capitalize(
              moment(`${form.form.month + 1}`, 'MM').format('MMMM')
            )
            setCurrentDate(`${currentMonth} del ${calculatedYear}`)

            dispatch(
              httpGet(
                DASHBOARD_GRAPH,
                {
                  month: form.form.month + 1,
                  year: form.form.year,
                },
                data => {
                  notification.info({
                    message: `${
                      data.total || 0
                    } movimientos se han enviado para su registro: para su seguimiento consulte la sección Movimientos AMIS ...`,
                    duration: 5,
                  })
                }
              )
            )
          }}
          onChange={({ value }, name) => dispatch(formChange(name, value))}
        />
        <GraphYear
          className="mt-4"
          name="GraphYear"
          data={http[DASHBOARD_GRAPH_HISTORY]}
          onExportPdf={onExportPdf}
          onSendEmail={onSendEmail}
          onRefresh={() => dispatch(httpGet(DASHBOARD_GRAPH_HISTORY))}
        />
        <h2 className="mt-4 mb-4">Últimos 5 Procesamientos</h2>
        {datasource && (
          <Table
            columns={[
              {
                title: 'Fecha y hora',
                dataIndex: 'fechaHora',
                sorter: (a, b) =>
                  moment(a.fechaHora).unix() - moment(b.fechaHora).unix(),
                sortOrder: sortOrder.field === 'fechaHora' && sortOrder.order,
                render: renderDate,
              },
              {
                title: 'Estatus',
                dataIndex: 'estatus',
                sorter: (a, b) => a.estatus.localeCompare(b.estatus),
                sortOrder: sortOrder.field === 'estatus' && sortOrder.order,
              },
              {
                title: 'Descripción',
                dataIndex: 'descripcion',
                sorter: (a, b) => a.descripcion.localeCompare(b.descripcion),
                sortOrder: sortOrder.field === 'descripcion' && sortOrder.order,
              },
              {
                title: 'Registros',
                dataIndex: 'registros',
                sorter: (a, b) => a.registros - b.registros,
                sortOrder: sortOrder.field === 'registros' && sortOrder.order,
              },
            ]}
            dataSource={datasource}
            onChange={onChange}
          />
        )}
        <FooterCar />
      </Card>
    </animated.div>
  )
}

export default Dashboard
