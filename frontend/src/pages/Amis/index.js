/**
 * Pagina  que se encarga de la manipulacion de Amis
 * @module pages/Amis
 */
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Card, DatePicker, Row, Col, Button, Input } from 'antd'
import { useSpring, animated } from 'react-spring'
import { useHistory } from 'react-router-dom'
import qs from 'qs'
import VisibilityIcon from '@material-ui/icons/Visibility'
import moment from 'moment'
import AssignmentReturnedIcon from '@material-ui/icons/AssignmentReturned'
import {
  FADE_TRANSITION,
  COLOR,
  DATE_DDMMYYYY,
  DATE_YYYYMMDD,
  DATE_DDMMYYYY2,
  HDI_FILTERS,
  INSURE_TYPE_NAME,
  TABLE_PAGE_CURREN_PAGE_ZISE,
  ORDER_STATUS,
} from '@utils/constants'
import uris from '@utils/uris'
import { urlParams, dateformat, downloadBlob } from '@utils'
import { FooterCar, Table } from '@components'
import { formInit, formUpdate, formChange } from '@actions/form'
import { httpPost, httpInit } from '@actions/http'
import { AMIS_SEARCH, AMIS_EXCEL } from '@utils/services'

/**
 * @hook
 */
const Amis = () => {
  const fade = useSpring(FADE_TRANSITION)
  const history = useHistory()
  const dispatch = useDispatch()
  const { http, form } = useSelector(state => state)
  const dataSource = http[AMIS_SEARCH] ? http[AMIS_SEARCH] : []
  const [sortOrder, setSortOrder] = useState({})
  const [urlExcel, setUrlExcel] = useState()

  const amis = {
    isBack: urlParams('isBack'),
    dateInit: urlParams('dateInit'),
    dateEnd: urlParams('dateEnd'),
    query: urlParams('query'),
    // eslint-disable-next-line radix
    currentPage: parseInt(urlParams('currentPage')),
    // eslint-disable-next-line radix
    currentPageSize: parseInt(urlParams('currentPageSize')),
    filter: urlParams('filter'),
  }

  function urlDowloadExcel(params) {
    setUrlExcel(params)
  }

  function dowloadExcel() {
    fetch(`/api${AMIS_EXCEL}`, {
      method: 'POST',
      body: JSON.stringify(urlExcel),
    })
      .then(resp => {
        return resp.blob()
      })
      .then(blob => {
        downloadBlob(
          blob,
          `Movimientos Amis ${moment(form.form.dateInit).format(
            DATE_DDMMYYYY2
          )} al ${moment(form.form.dateEnd).format(DATE_DDMMYYYY2)}.xls`
        )
      })
  }

  useEffect(() => {
    // form
    if (amis.isBack === 'true') {
      dispatch(
        formInit({
          dateInit: amis.dateInit,
          dateEnd: amis.dateEnd,
          query: amis.query,
          currentPage: amis.currentPage,
          currentPageSize: amis.currentPageSize,
          filter: amis.filter ? JSON.parse(amis.filter) : { status: [] },
          offset: (amis.currentPage - 1) * amis.currentPageSize,
        })
      )

      const params = {
        from: amis.dateInit,
        to: amis.dateEnd,
        offset: amis.currentPage,
        fetch: amis.currentPageSize,
        order: 'DESC',
      }

      if (amis.query && amis.query.length > 0) {
        params.query = amis.query
      }

      const filters = JSON.parse(amis.filter)
      if (filters && filters.status) {
        const filtersName = HDI_FILTERS.filter(x =>
          filters.status.includes(x.value)
        )
        const filterId = filtersName.map(x => x.id)
        if (filterId.length > 0) params.filter = filterId
      }

      dispatch(httpPost(AMIS_SEARCH, params))

      // excel
      urlDowloadExcel(params)
    } else {
      dispatch(
        formInit({
          dateInit: '',
          dateEnd: '',
          query: '',
          currentPage: 1,
          currentPageSize: TABLE_PAGE_CURREN_PAGE_ZISE,
          filter: { status: [] },
        })
      )
    }

    // http
    dispatch(httpInit())
  }, [])

  const onChange = (evt, name) => {
    if (name === 'date') {
      if (evt) {
        dispatch(
          formUpdate({
            dateInit: evt[0].format(DATE_YYYYMMDD),
            dateEnd: evt[1].format(DATE_YYYYMMDD),
          })
        )
      } else {
        dispatch(
          formUpdate({
            dateInit: null,
            dateEnd: null,
          })
        )
      }
    } else if (name === 'search') {
      dispatch(formChange('query', evt.target.value))
    }
  }

  const onSearch = () => {
    const {
      form: { dateInit, dateEnd, query },
    } = form
    const params = {
      from: dateInit,
      to: dateEnd,
      offset: 1,
      fetch: TABLE_PAGE_CURREN_PAGE_ZISE,
      order: 'DESC',
    }

    if (query && query.length > 0) {
      params.query = query
    }

    dispatch(httpPost(AMIS_SEARCH, params))

    // excel
    urlDowloadExcel(params)

    dispatch(
      formUpdate({
        currentPage: 1,
        currentPageSize: TABLE_PAGE_CURREN_PAGE_ZISE,
        filter: { status: [] },
      })
    )

    setSortOrder({})
  }

  const onChangeTable = (page, filters, sorter) => {
    const {
      form: { dateInit, dateEnd, query },
    } = form

    const params = {
      from: dateInit,
      to: dateEnd,
      offset: page.current,
      fetch: page.pageSize,
      order: 'DESC',
    }

    //	"order":"ASC|DESC"
    if (sorter.order === 'ascend') params.order = 'ASC'
    else if (sorter.order === 'descend') params.order = 'DESC'

    if (query && query.length > 0) {
      params.query = query
    }

    if (filters && filters.status) {
      const filtersName = HDI_FILTERS.filter(x =>
        filters.status.includes(x.value)
      )
      const filterId = filtersName.map(x => x.id)
      if (filterId.length > 0) params.filter = filterId
    }

    if (sorter.column) {
      params.column = ORDER_STATUS[sorter.column.dataIndex]
    }

    dispatch(httpPost(AMIS_SEARCH, params))

    // excel
    urlDowloadExcel(params)

    setSortOrder(sorter)

    dispatch(
      formUpdate({
        currentPage: page.current,
        currentPageSize: page.pageSize,
        filter: filters && filters.status ? filters : { status: [] },
      })
    )
  }

  const renderAction = ({ movimientoId, insureType, status, date }) => (
    <VisibilityIcon
      style={{ color: COLOR.info, cursor: 'pointer' }}
      onClick={() => {
        history.push(
          `${uris.amisDeatil.index.uri}?${qs.stringify({
            movimientoId,
            insureType,
            date,
            status: HDI_FILTERS.filter(x => x.value === status)[0].text,
            // AMIS
            dateInit: form.form.dateInit,
            dateEnd: form.form.dateEnd,
            query: form.form.query,
            currentPage: form.form.currentPage,
            currentPageSize: form.form.currentPageSize,
            filter: JSON.stringify(form.form.filter),
          })}`
        )
      }}
    />
  )

  const renderColumn = text => {
    const str = HDI_FILTERS.filter(x => x.value === text)[0].text

    if (str.split(' ')[0] === 'Error') {
      return <span className="text-red">{str}</span>
    }
    return <span>{str}</span>
  }
  const renderInsureType = id => <span>{INSURE_TYPE_NAME[id]}</span>

  const renderDate = date => <span>{dateformat(date)}</span>

  const [dates, setDates] = useState([])
  const disabledDate = current => {
    if (!dates || dates.length === 0) {
      return current > moment().endOf('day')
    }
    const thenSelectedDate =
      (dates[0] && current.diff(dates[0], 'days') > 30) ||
      current > moment().endOf('day')
    return thenSelectedDate
  }

  return (
    <>
      <animated.div style={fade}>
        <Card>
          <Row>
            <Col span={20}>
              <h1>Movimientos Generados para AMIS</h1>
            </Col>
            <Col span={4}>
              <Button
                id="download"
                type="secondary"
                shape="circle"
                className="btn-circle"
                style={{ float: 'right' }}
                icon={<AssignmentReturnedIcon />}
                disabled={!dataSource.data}
                onClick={() => dowloadExcel()}
                download
              />
            </Col>
          </Row>

          <h4>Buscar movimientos por fecha de extracción</h4>
          <h6>Seleccione un rango de fechas</h6>

          <Row className="mb-2">
            <Col span={12}>
              <Row>
                <Col span={12}>
                  <DatePicker.RangePicker
                    disabledDate={disabledDate}
                    onCalendarChange={value => {
                      setDates(value)
                    }}
                    format={DATE_DDMMYYYY}
                    onChange={e => onChange(e, 'date')}
                    defaultValue={() => {
                      if (!amis.dateInit && !amis.dateEnd) {
                        return []
                      }

                      return [
                        moment(
                          moment(amis.dateInit, 'YYYYMMDD').format(
                            DATE_DDMMYYYY
                          ),
                          DATE_DDMMYYYY
                        ),
                        moment(
                          moment(amis.dateEnd, 'YYYYMMDD').format(
                            DATE_DDMMYYYY
                          ),
                          DATE_DDMMYYYY
                        ),
                      ]
                    }}
                  />
                </Col>
                <Col span={11} offset={1}>
                  <Button
                    id="btnSearch"
                    className="btn-gray"
                    onClick={onSearch}
                    disabled={!form.form.dateInit && !form.form.dateEnd}
                  >
                    Buscar
                  </Button>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row className="mb-3 mt-4">
            <Col span={16}>
              <h5 className="mt-2 mb-0">
                {form.form.dateInit && (
                  <>
                    Movimientos del{' '}
                    {moment(form.form.dateInit).format(DATE_DDMMYYYY)} al{' '}
                    {moment(form.form.dateEnd).format(DATE_DDMMYYYY)}
                  </>
                )}
              </h5>
            </Col>
            <Col span={8}>
              <Input.Search
                placeholder="Buscar registro"
                onChange={evt => onChange(evt, 'search')}
                onSearch={onSearch}
                value={form.form.query}
              />
            </Col>
          </Row>

          {form.form.filter && (
            <Table
              dataSource={dataSource.data}
              columns={[
                {
                  title: 'No. Póliza',
                  dataIndex: 'policy',
                  sorter: (a, b) => a.policy.localeCompare(b.policy),
                  sortOrder: sortOrder.field === 'policy' && sortOrder.order,
                },
                {
                  title: 'Agente',
                  dataIndex: 'agent',
                  sorter: (a, b) => a.agent - b.agent,
                  sortOrder: sortOrder.field === 'agent' && sortOrder.order,
                },
                {
                  title: 'Tipo Póliza',
                  dataIndex: 'insureType',
                  render: renderInsureType,
                  sorter: (a, b) => a.insureType.localeCompare(b.insureType),
                  sortOrder:
                    sortOrder.field === 'insureType' && sortOrder.order,
                },
                {
                  title: 'VIN',
                  dataIndex: 'vin',
                  sorter: (a, b) => a.vin.localeCompare(b.vin),
                  sortOrder: sortOrder.field === 'vin' && sortOrder.order,
                },
                {
                  title: 'Nombre o Razón Social',
                  dataIndex: 'bussinessName',
                  sorter: (a, b) =>
                    a.bussinessName.localeCompare(b.bussinessName),
                  sortOrder:
                    sortOrder.field === 'bussinessName' && sortOrder.order,
                },
                {
                  title: 'Fecha de extracción',
                  dataIndex: 'date',
                  render: renderDate,
                  sorter: (a, b) => a.date - b.date,
                  sortOrder: sortOrder.field === 'date' && sortOrder.order,
                },
                {
                  title: 'Estatus',
                  dataIndex: 'status',
                  render: renderColumn,
                  filters: HDI_FILTERS,
                  filteredValue: form.form.filter.status || null,
                  sorter: (a, b) => a.status.localeCompare(b.status),
                  sortOrder: sortOrder.field === 'status' && sortOrder.order,
                },
                {
                  title: '',
                  render: renderAction,
                },
              ]}
              pagination={{
                current: form.form.currentPage,
                pageSize: form.form.currentPageSize,
                total: dataSource.total,
              }}
              onChange={onChangeTable}
            />
          )}

          <FooterCar />
        </Card>
      </animated.div>
    </>
  )
}

export default Amis
