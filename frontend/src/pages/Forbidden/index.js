/**
 * Pagina para mostrar para recursos no permitidos
 * @module pages/Forbidden
 */
import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Divider, Card, Col, Row } from 'antd'

/**
 * @hook
 */
const Forbidden = () => {
  return (
    <>
      <Divider orientation="left">
        <h1>
          <FormattedMessage id="forbidden.title" />
        </h1>
      </Divider>

      <Card size="small">
        <Row gutter={24} className="m-3">
          <Col span={24} style={{ textAlign: 'center' }}>
            <h2
              className="text-error"
              style={{ fontWeight: 'bold', fontSize: '36px' }}
            >
              403
            </h2>

            <h2>
              <FormattedMessage id="forbidden.title" />
            </h2>

            <p>
              <FormattedMessage id="forbidden.msg" />
            </p>
          </Col>
        </Row>
      </Card>
    </>
  )
}

export default Forbidden
