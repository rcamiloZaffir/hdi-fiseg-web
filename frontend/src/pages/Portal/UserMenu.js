/**
 * Componente web de la Pagina del usuario
 * @module pages/UserMenu
 */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react'
import { Popover } from 'antd'
import FaceIcon from '@material-ui/icons/Face'
import Cookies from 'js-cookie'
import { LOGOUT } from '@utils/services'

/**
 * @cosntructor
 * @param {JSON} params del userName en contexto
 */
export default function userMenu({ userName, email }) {
  const [visible, setVisible] = useState(false)

  const logout = () => {
    Cookies.remove('token')
    window.location.href = LOGOUT
  }

  return (
    <div className="user-menu">
      <div className="account">
        <p className="user"> {userName} </p>
        <p className="rol">{email}</p>
      </div>

      <Popover
        content={
          <a id="logout" href="#" onClick={() => logout()}>
            Cerrar sesión
          </a>
        }
        trigger="click"
        visible={visible}
        onVisibleChange={() => setVisible(!visible)}
      >
        <FaceIcon className="user-icon" />
      </Popover>
    </div>
  )
}
