/**
 * Componente web de la Pagina del menu
 * @module pages/TopMenu
 */
import React from 'react'
import { Menu } from 'antd'
import { useHistory, useLocation } from 'react-router-dom'

/**
 * @hook
 */
export default function TopMenu() {
  const history = useHistory()
  const location = useLocation()

  return (
    <Menu
      mode="horizontal"
      onClick={({ key }) => history.push(`${key}`)}
      selectedKeys={location.pathname}
    >
      <Menu.Item key="/dashboard">Dashboard</Menu.Item>
      <Menu.Item key="/amis">Movimientos AMIS</Menu.Item>
    </Menu>
  )
}
