/**
 * Pagina que se encarga de mostrar el portal web
 * @module pages/Portal
 */
import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import ErrorHandler from '@components/ErrorHandler'
import Spinner from '@components/Spinner'
import { FormattedMessage } from 'react-intl'
import { useHistory, Link } from 'react-router-dom'
import { Layout, Row, Col, Input } from 'antd'
import uris from '@utils/uris'
import qs from 'qs'
import { isAuthenticated, urlParams } from '@utils'
import UserMenu from './UserMenu'
import TopMenu from './TopMenu'

const { Content, Footer } = Layout

/**
 * @cosntructor
 * @param {*} params para inyectar las paginas web
 */
const Portal = ({ children }) => {
  const state = useSelector(currentState => currentState)
  const history = useHistory()
  const { errorHandler } = state
  const [valueSearch, setValueSearch] = useState(urlParams('search'))

  const handleSearch = value => {
    history.push(`${uris.search.index.uri}?${qs.stringify({ search: value.trim() })}`)
  }

  return (
    <>
      <Spinner />
      <ErrorHandler className="error-handler" errorHandler={errorHandler} />

      <Layout style={{ minHeight: '100vh' }}>
        {isAuthenticated() ? (
          <>
            <div className="header">
              <Row gutter={24}>
                <Col span={18} push={3} pull={3} className="bg">
                  <Link
                    href="/dashboard"
                    to="/dashboard"
                    onClick={() => setValueSearch('')}
                  >
                    <img className="logo" src="/img/logo.png" alt="bg-login" />
                  </Link>
                  <UserMenu
                    userName={isAuthenticated().username}
                    email={isAuthenticated().email}
                  />

                  <Row gutter={24}>
                    <Col span={15}>
                      <TopMenu />
                    </Col>
                    <Col span={8}>
                      <div className="input-search">
                        <Input.Search
                          placeholder="Consulta Global de Póliza AMIS"
                          value={valueSearch}
                          onChange={evt => {
                            setValueSearch(evt.currentTarget.value.trim())
                          }}
                          onSearch={value => handleSearch(value)}
                          size="large"
                          maxLength={17}
                        />
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>

            <Layout className="center-iw content">
              <Content
                style={{
                  width: '70%',
                }}
              >
                {children}
              </Content>
            </Layout>
            <Footer>
              <FormattedMessage id="footer.content" />
            </Footer>
          </>
        ) : (
          <>
            <Layout className="center-iw content">
              <Content
                style={{
                  height: 'calc( 100vh - 600px )',
                  overflowY: 'auto',
                  width: '70%',
                }}
              >
                {children}
              </Content>
            </Layout>

            <Footer>
              <FormattedMessage id="footer.content" />
            </Footer>
          </>
        )}
      </Layout>
    </>
  )
}

export default Portal
