/**
 * Pagina para recursos no econtrados
 * @module pages/NotFound
 */
import React from 'react'
import { FormattedMessage } from 'react-intl'
import { Divider, Card, Col, Row } from 'antd'

/**
 * @hook
 */
const NotFound = () => {
  return (
    <>
      <Divider orientation="left">
        <h1>
          <FormattedMessage id="notfound" />
        </h1>
      </Divider>

      <Card size="small">
        <Row gutter={24} className="m-3">
          <Col span={24} style={{ textAlign: 'center' }}>
            <h2
              className="text-error"
              style={{ fontWeight: 'bold', fontSize: '36px' }}
            >
              404
            </h2>

            <h2>
              <FormattedMessage id="notfound" />
            </h2>
          </Col>
        </Row>
      </Card>
    </>
  )
}

export default NotFound
