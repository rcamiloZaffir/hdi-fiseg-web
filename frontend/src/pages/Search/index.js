/**
 * Pagina para buscar polizas
 * @module pages/Search
 */
import React, { useEffect, useState } from 'react'
import { Card, Avatar, Row, Col, Button, Badge, notification } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { useSpring, animated } from 'react-spring'
import AssignmentReturnedIcon from '@material-ui/icons/AssignmentReturned'
import MailOutlineIcon from '@material-ui/icons/MailOutline'
import AssignmentLateIcon from '@material-ui/icons/AssignmentLate'
import { FooterCar, Table } from '@components'
import { FADE_TRANSITION, COLOR, GET } from '@utils/constants'
import { httpGet, httpInit, httpPost } from '@actions/http'
import {
  SEARCH,
  SEARCH_POLICY_VEHICLES,
  SEARCH_EXCEL,
  SEARCH_MAIL_TO,
} from '@utils/services'
import { formInit, formUpdate } from '@actions/form'
import { urlParams, dateformat, isAuthenticated } from '@utils'
import { buildUrlBase } from '@utils/httpService'

const mailActive = process.env.REACT_APP_SEND_MAIL_ACTIVE_FE

/**
 * @hook
 */
const Search = () => {
  const fade = useSpring(FADE_TRANSITION)
  const dispatch = useDispatch()
  const { http, form } = useSelector(state => state)

  const policyOrVin = urlParams('search')
  const SEARCH_POLICY = `${SEARCH}/${policyOrVin}`
  const SEARCH_POLICY_VEHICLES_POLICYID = `${SEARCH_POLICY_VEHICLES}/${policyOrVin}`

  let dataSearchPolicy
  let dataMovimientoVehicles

  const [sortOrder, setSortOrder] = useState({})
  const [sortOrder2, setSortOrder2] = useState({})
  const [validation, setValidation] = useState()

  const getPolizaOrVin = e => (e === 'POLIZA' ? 'poliza' : 'vin')

  if (policyOrVin) {
    dataSearchPolicy = http[SEARCH_POLICY] ?? false
    dataMovimientoVehicles = http[SEARCH_POLICY_VEHICLES_POLICYID] ?? false
  }

  useEffect(() => {
    // form
    dispatch(
      formInit({
        query: '',
        offset: 1,
        fetch: 15,
        response: 0,
      })
    )

    // http
    dispatch(httpInit())

    if (policyOrVin) {
      if (
        /^[a-zA-Z0-9-\s]+$/g.test(policyOrVin) &&
        policyOrVin.length >= 5 &&
        policyOrVin.length <= 17
      ) {
        setValidation('')
        dispatch(httpGet(SEARCH_POLICY))
      } else {
        setValidation('VIN o Póliza invalida')
      }
    }
  }, [policyOrVin])

  useEffect(() => {
    if (dataSearchPolicy) {
      dispatch(
        httpGet(SEARCH_POLICY_VEHICLES_POLICYID, {
          query: getPolizaOrVin(dataSearchPolicy.tipoBusquedaFinal),
          offset: 1,
          fetch: 15,
        })
      )
    }
  }, [dataSearchPolicy])

  const onSendEmail = () => {
    dispatch(
      httpPost(
        `${SEARCH_MAIL_TO}/${policyOrVin}`,
        {
          query: getPolizaOrVin(dataSearchPolicy.tipoBusquedaFinal),
          offset: form.form.offset,
          fetch: form.form.fetch,
          email: isAuthenticated().email,
        },
        () => {
          notification.info({
            message: 'Email enviado correctamente',
            description: `Se envio el archivo de excel `,
            duration: 5,
          })
        }
      )
    )
  }

  const onChangeTable = (page, filters, sorter) => {
    setSortOrder(sorter)
  }

  const onChangeTable2 = (page, filters, sorter) => {
    const { current, pageSize } = page

    dispatch(
      httpGet(SEARCH_POLICY_VEHICLES_POLICYID, {
        query: getPolizaOrVin(dataSearchPolicy.tipoBusquedaFinal),
        offset: current,
        fetch: pageSize,
      })
    )

    dispatch(
      formUpdate({
        offset: current,
        fetch: pageSize,
      })
    )

    setSortOrder2(sorter)
  }

  const Status = ({ status }) => {
    let color = COLOR.error

    if (status === 'VIGENTE') {
      color = COLOR.primary
    }

    return <Badge count={status} style={{ backgroundColor: color }} />
  }

  const MySearch = () => (
    <>
      <Row>
        <Col span={16}>
          <h1>Consulta Global Pólizas AMIS</h1>
        </Col>
        <Col span={8}>
          <div style={{ float: 'right' }}>
            <Button
              id="download"
              type="secondary"
              shape="circle"
              className="btn-circle"
              icon={<AssignmentReturnedIcon />}
              href={buildUrlBase(
                `${SEARCH_EXCEL}/${policyOrVin}`,
                {
                  query: getPolizaOrVin(dataSearchPolicy.tipoBusquedaFinal),
                  offset: form.form.offset,
                  fetch: form.form.fetch,
                },
                GET
              )}
              download
            />
            {mailActive === 'true' && (
              <Button
                id="email"
                type="secondary"
                shape="circle"
                className="btn-circle"
                icon={<MailOutlineIcon />}
                onClick={onSendEmail}
              />
            )}
          </div>
        </Col>
      </Row>
      <Row className="mb-3">
        <Col span="16">
          <h2 className="color-primary">
            Núm. de {getPolizaOrVin(dataSearchPolicy.tipoBusquedaFinal)}:{' '}
            {policyOrVin} <Status status={dataSearchPolicy.estatus} />
          </h2>
          <h5 className="color-secondary">
            Fecha de emisión:{' '}
            {dataSearchPolicy.issueDate &&
              dateformat(dataSearchPolicy.issueDate)}
          </h5>
          <h5 className="color-secondary">
            Fecha de inicio de vigencia:{' '}
            {dataSearchPolicy.initDate && dateformat(dataSearchPolicy.initDate)}
          </h5>
          <h5 className="color-secondary">
            Fecha de final de vigencia:{' '}
            {dataSearchPolicy.endDate && dateformat(dataSearchPolicy.endDate)}
          </h5>
        </Col>
        <Col span="8">
          <h5 className="color-secondary">
            Tipo de movimiento: {dataSearchPolicy.moveType}
          </h5>
        </Col>
      </Row>
      <h4 className="mt-4 mb-4">Cobertura</h4>
      <Table
        dataSource={dataSearchPolicy.coverage}
        onChange={onChangeTable}
        columns={[
          {
            title: 'Id',
            dataIndex: 'id',
            sorter: (a, b) => a.id - b.id,
            sortOrder: sortOrder.field === 'id' && sortOrder.order,
          },
          {
            title: 'Id Suma Asegurada',
            dataIndex: 'insuranceSumId',
            sorter: (a, b) => a.insuranceSumId - b.insuranceSumId,
            sortOrder: sortOrder.field === 'insuranceSumId' && sortOrder.order,
          },
          {
            title: 'Suma Asegurada',
            dataIndex: 'insuranceSum',
            sorter: (a, b) => a.insuranceSum - b.insuranceSum,
            sortOrder: sortOrder.field === 'insuranceSum' && sortOrder.order,
          },
          {
            title: 'Estatus',
            dataIndex: 'status',
            sorter: (a, b) => a.status - b.status,
            sortOrder: sortOrder.field === 'status' && sortOrder.order,
          },
        ]}
      />
      <Avatar
        style={{
          color: 'white',
          backgroundColor: COLOR.primary,
          margin: '5px',
          padding: '5px',
        }}
        src="img/car-single2.svg"
      />
      <span
        style={{
          color: COLOR.primary,
          fontWeight: 'bold',
        }}
      >
        Vehículo(s)
      </span>

      <Table
        dataSource={dataMovimientoVehicles.data}
        onChange={onChangeTable2}
        pagination={{
          current: form.form.offset,
          pageSize: form.form.fetch,
          total: dataMovimientoVehicles.total,
        }}
        columns={[
          {
            title: '#',
            dataIndex: 'vehiculoId',
            sorter: (a, b) => a.vehiculoId - b.vehiculoId,
            sortOrder: sortOrder2.field === 'vehiculoId' && sortOrder2.order,
          },
          {
            title: 'VIN',
            dataIndex: 'vin',
            sorter: (a, b) => a.vin - b.vin,
            sortOrder: sortOrder2.field === 'vin' && sortOrder2.order,
          },
          {
            title: 'Uso',
            dataIndex: 'use',
            sorter: (a, b) => a.use - b.use,
            sortOrder: sortOrder2.field === 'use' && sortOrder2.order,
          },
          {
            title: 'Obligatorio',
            dataIndex: 'required',
            sorter: (a, b) => a.required - b.required,
            sortOrder: sortOrder2.field === 'required' && sortOrder2.order,
          },
          {
            title: 'Modelo',
            dataIndex: 'model',
            sorter: (a, b) => a.model - b.model,
            sortOrder: sortOrder2.field === 'model' && sortOrder2.order,
          },
          {
            title: 'Placa',
            dataIndex: 'licensePlate',
            sorter: (a, b) => a.licensePlate - b.licensePlate,
            sortOrder: sortOrder2.field === 'licensePlate' && sortOrder2.order,
          },
          {
            title: 'Inciso',
            dataIndex: 'inciso',
            sorter: (a, b) => a.inciso - b.inciso,
            sortOrder: sortOrder2.field === 'inciso' && sortOrder2.order,
          },
          {
            title: 'Marca',
            dataIndex: 'brand',
            sorter: (a, b) => a.brand - b.brand,
            sortOrder: sortOrder2.field === 'brand' && sortOrder2.order,
          },
          {
            title: 'Cliente2*',
            dataIndex: 'client2',
            sorter: (a, b) => a.client2 - b.client2,
            sortOrder: sortOrder2.field === 'client2' && sortOrder2.order,
          },
          {
            title: 'Beneficiario*',
            dataIndex: 'beneficiary',
            sorter: (a, b) => {
              return (
                (a.beneficiary ? a.beneficiary.length : 0) -
                (b.beneficiary ? b.beneficiary.length : 0)
              )
            },
            sortOrder: sortOrder2.field === 'beneficiary' && sortOrder2.order,
          },
        ]}
      />
    </>
  )

  const Validation = ({ msg }) => (
    <>
      <span className="color-primary pr-2">
        <AssignmentLateIcon />
      </span>
      <h4 style={{ fontSize: '18px' }}>{msg}</h4>
    </>
  )

  return (
    <animated.div style={fade}>
      <Card>
        {dataSearchPolicy ? (
          <MySearch />
        ) : (
          <>
            <Row>
              <Col span={16}>
                <h1>Consulta Global Pólizas AMIS</h1>
              </Col>
            </Row>

            <Row className="pt-5 pb-5">
              <Col span={24} className="center-iw">
                {dataSearchPolicy === '' && (
                  <Validation msg="No se encontró ninguna póliza registrada para su consulta." />
                )}

                {(policyOrVin.length === 0 || validation) && (
                  <Validation msg="Póliza o VIN inválido." />
                )}
              </Col>
            </Row>
          </>
        )}

        <FooterCar />
      </Card>
    </animated.div>
  )
}
export default Search
