/**
 * Pagina par amostrar el detalle de las AMis
 * @module pages/AmisDetail
 */
import React, { useState, useEffect } from 'react'
import { Card, Form, Row, Col, Modal, Alert, Input, Avatar } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { useSpring, animated } from 'react-spring'
import VisibilityIcon from '@material-ui/icons/Visibility'
import {
  FADE_TRANSITION,
  COLOR,
  INSURE_TYPE_NAME,
  INSURE_TYPE,
} from '@utils/constants'
import uris from '@utils/uris'
import { urlParams, dateformat, number } from '@utils'
import { FooterCar, IconBack, Table, Slider } from '@components'
import { httpGet, httpInit } from '@actions/http'
import { formInit, formChange } from '@actions/form'
import {
  AMIS_MOVIMIENTOS,
  AMIS_MOVIMIENTOS_VEHICLES,
  AMIS_MOVIMIENTOS_FLOTILLA_DETALLE,
} from '@utils/services'
import qs from 'qs'

/**
 * @hook
 */
const AmisDetail = () => {
  const fade = useSpring(FADE_TRANSITION)
  const [modal, setModal] = useState(false)
  const [modalItem, setModalItem] = useState()
  const [amisFlotillaDetalle, setAmisFlotillaDetalle] = useState(false)
  const dispatch = useDispatch()
  const { http, form } = useSelector(state => state)

  const movimientoId = urlParams('movimientoId')
  // eslint-disable-next-line radix
  const insureType = parseInt(urlParams('insureType'))
  const status = urlParams('status')
  const date = urlParams('date')
  const amis = {
    dateInit: urlParams('dateInit'),
    dateEnd: urlParams('dateEnd'),
    query: urlParams('query'),
    currentPage: urlParams('currentPage'),
    currentPageSize: urlParams('currentPageSize'),
    filter: urlParams('filter'),
  }

  const AMIS_MOVIMIENTOS_MOVIMIENTOID = `${AMIS_MOVIMIENTOS}/${movimientoId}`
  const data = http[AMIS_MOVIMIENTOS_MOVIMIENTOID] ?? false

  const AMIS_MOVIMIENTOS_VEHICLES_MOVIMIENTOID = `${AMIS_MOVIMIENTOS_VEHICLES}/${movimientoId}`
  const dataMovimientoVehicles =
    http[AMIS_MOVIMIENTOS_VEHICLES_MOVIMIENTOID] ?? false

  if (dataMovimientoVehicles) {
    dataMovimientoVehicles.data = dataMovimientoVehicles.data.map((x, e) => {
      return { ...x, ...{ id: e + 1 } }
    })
  }

  const dataAmisFlotillaDetalle = http[amisFlotillaDetalle]
  const [sortOrder, setSortOrder] = useState({})

  useEffect(() => {
    // form
    dispatch(
      formInit({
        query: '',
        offset: 1,
        fetch: 15,
      })
    )

    // http
    dispatch(httpInit())

    dispatch(httpGet(AMIS_MOVIMIENTOS_MOVIMIENTOID))
    if (insureType === INSURE_TYPE.FLOTILLA)
      dispatch(
        httpGet(AMIS_MOVIMIENTOS_VEHICLES_MOVIMIENTOID, {
          offset: 1,
          fetch: 15,
        })
      )
  }, [])

  const onSearch = () => {
    const {
      form: { query, fetch },
    } = form

    const params = {
      offset: 1,
      fetch,
    }

    if (query && query.length > 0) {
      params.query = query
    }

    dispatch(httpGet(AMIS_MOVIMIENTOS_VEHICLES_MOVIMIENTOID, params))
  }

  const onChange = (evt, name) => {
    if (name === 'search') {
      dispatch(formChange('query', evt.target.value))
    }
  }

  const onChangeTable = (page, filters, sorter) => {
    const {
      form: { query },
    } = form

    const params = {
      offset: page.current,
      fetch: page.pageSize,
    }

    if (query && query.length > 0) {
      params.query = query
    }

    dispatch(httpGet(AMIS_MOVIMIENTOS_VEHICLES_MOVIMIENTOID, params))

    setSortOrder(sorter)
  }

  const onChangeTable2 = (page, filters, sorter) => {
    setSortOrder(sorter)
  }

  function showModal(item) {
    setModalItem(item)
    setModal(true)
  }

  function hideModal() {
    setModal(false)
  }

  const renderAction = item => (
    <VisibilityIcon
      style={{ color: COLOR.info, cursor: 'pointer' }}
      onClick={() => {
        setAmisFlotillaDetalle(AMIS_MOVIMIENTOS_FLOTILLA_DETALLE)
        dispatch(
          httpGet(
            AMIS_MOVIMIENTOS_FLOTILLA_DETALLE,
            {
              movimientoId,
              vin: item.vin,
              vehiculoId: item.vehiculoId,
            },
            () => showModal(item)
          )
        )
      }}
    />
  )

  return (
    <>
      <animated.div style={fade}>
        <Card>
          <Row>
            <Col span={20}>
              <h1>Estatus de procesamiento</h1>
            </Col>
            <Col span={4}>
              <IconBack
                to={`${uris.amis.index.uri}?${qs.stringify({
                  isBack: true,
                  dateInit: amis.dateInit,
                  dateEnd: amis.dateEnd,
                  query: amis.query,
                  currentPage: amis.currentPage,
                  currentPageSize: amis.currentPageSize,
                  filter: amis.filter,
                })}`}
              />
            </Col>
          </Row>

          {data && (
            <>
              <Alert
                message={`Movimiento de Póliza ${data.id} solicitud en ${status} ...`}
                type="success"
              />
              <Row className="m-5">
                <Col span={12} offset={6}>
                  <Slider statusName={status} />
                </Col>
              </Row>
              <Row className="mb-3">
                <Col span="12">
                  <h4>Movimiento de Póliza {INSURE_TYPE_NAME[insureType]}</h4>
                  <h4>Extraído del {dateformat(date)}</h4>
                  <small>
                    * Información solo de consulta, no será enviado para su
                    registro en AMIS
                  </small>
                </Col>
              </Row>
              <Row>
                <Col lg={12} md={24}>
                  <Row className="form-data">
                    <Col lg={12} md={24}>
                      <h4>Datos del seguro</h4>
                      <Form labelAlign="left">
                        <Form.Item label="Tipo de seguro">
                          {data.insuranceData.typeInsurance}
                        </Form.Item>
                        <Form.Item label="Núm de Póliza">
                          {data.insuranceData.policyNumber}
                        </Form.Item>
                        <Form.Item label="Fecha de Emisión">
                          {dateformat(data.insuranceData.issueDate)}
                        </Form.Item>
                        <Form.Item label="Tipo de Movimiento">
                          {data.insuranceData.movementType}
                        </Form.Item>
                        <Form.Item label="Inicio Vigencia">
                          {dateformat(data.insuranceData.startValidity)}
                        </Form.Item>
                        <Form.Item label="Final Vigencia">
                          {dateformat(data.insuranceData.finalValidity)}
                        </Form.Item>
                      </Form>
                    </Col>

                    <Col lg={12} md={24}>
                      <h4>Cliente</h4>
                      <Form className="form-data" labelAlign="left">
                        <Form.Item label="Apellido Paterno">
                          {data.client.lastName}
                        </Form.Item>
                        <Form.Item label="Apellido Materno">
                          {data.client.secondLastName}
                        </Form.Item>
                        <Form.Item label="Nombre(s)">
                          {data.client.name}
                        </Form.Item>
                        <Form.Item label="*CURP">{data.client.curp}</Form.Item>
                        <Form.Item label="RFC">{data.client.rfc}</Form.Item>
                      </Form>
                    </Col>
                  </Row>
                </Col>

                <Col lg={12} md={24}>
                  <Row className="form-data">
                    <Col lg={12} md={24}>
                      <h4>Beneficiario</h4>
                      <Form labelAlign="left">
                        <Form.Item label="*Apellido Paterno">
                          {data.beneficiary.lastName}
                        </Form.Item>
                        <Form.Item label="*Apellido Materno">
                          {data.beneficiary.secondLastName}
                        </Form.Item>
                        <Form.Item label="*Nombre(s)">
                          {data.beneficiary.name}
                        </Form.Item>
                        <Form.Item label="*CURP">
                          {data.beneficiary.curp}
                        </Form.Item>
                        <Form.Item label="*RFC">
                          {data.beneficiary.rfc}
                        </Form.Item>
                      </Form>
                    </Col>

                    <Col lg={12} md={24}>
                      <h4>Canal de Ventas</h4>
                      <Form className="form-data" labelAlign="left">
                        <Form.Item label="Canal de ventas">
                          {data.salesChannel.salesChannel}
                        </Form.Item>
                        <Form.Item label="Agencia">
                          {data.salesChannel.agency}
                        </Form.Item>
                        <Form.Item label="Agente">
                          {data.salesChannel.agent}
                        </Form.Item>
                        <Form.Item label="Ubicación asegurado">
                          {data.salesChannel.locationInsured}
                        </Form.Item>
                        <Form.Item label="*Ubicación Agente">
                          {data.salesChannel.agentLocation}
                        </Form.Item>
                      </Form>
                    </Col>
                  </Row>
                </Col>
              </Row>

              {insureType === INSURE_TYPE.INDIVIDUAL && (
                <>
                  <h4 className="mt-2 mb-4">Cobertura</h4>
                  <Table
                    dataSource={data.coverage}
                    columns={[
                      {
                        title: 'Id',
                        dataIndex: 'id',
                        sorter: (a, b) => a.id - b.id,
                        sortOrder: sortOrder.field === 'id' && sortOrder.order,
                      },
                      {
                        title: 'Id Suma Asegurada',
                        dataIndex: 'insuranceSumId',
                        sorter: (a, b) => a.insuranceSumId - b.insuranceSumId,
                        sortOrder:
                          sortOrder.field === 'insuranceSumId' &&
                          sortOrder.order,
                      },
                      {
                        title: 'Suma Asegurada',
                        dataIndex: 'insuranceSum',
                        sorter: (a, b) => a.insuranceSum - b.insuranceSum,
                        sortOrder:
                          sortOrder.field === 'insuranceSum' && sortOrder.order,
                      },
                      {
                        title: 'Estatus',
                        dataIndex: 'status',
                        sorter: (a, b) => a.status - b.status,
                        sortOrder:
                          sortOrder.field === 'status' && sortOrder.order,
                      },
                    ]}
                    onChange={onChangeTable2}
                  />
                </>
              )}

              {insureType === INSURE_TYPE.FLOTILLA && (
                <>
                  <Row className="mb-4 mt-4">
                    <Col span={2} style={{ textAlign: 'center' }}>
                      <Avatar
                        style={{ backgroundColor: COLOR.primary }}
                        icon={<img src="/img/car-single2.svg" alt="" />}
                      />
                    </Col>
                    <Col span={14}>
                      <h4 className="mt-2 mb-0">Vehiculo(s)</h4>
                    </Col>
                    <Col span={8}>
                      <Input.Search
                        placeholder="Buscar registro"
                        onChange={evt => onChange(evt, 'search')}
                        onSearch={onSearch}
                      />
                    </Col>
                  </Row>
                  <Table
                    dataSource={dataMovimientoVehicles.data}
                    columns={[
                      {
                        title: '#',
                        dataIndex: 'id',
                        sorter: (a, b) => a.id - b.id,
                        sortOrder: sortOrder.field === 'id' && sortOrder.order,
                      },
                      {
                        title: 'VIN',
                        dataIndex: 'vin',
                        sorter: (a, b) => a.vin.localeCompare(b.vin),
                        sortOrder: sortOrder.field === 'vin' && sortOrder.order,
                      },
                      {
                        title: 'Uso',
                        dataIndex: 'use',
                        sorter: (a, b) => a.use - b.use,
                        sortOrder: sortOrder.field === 'use' && sortOrder.order,
                      },
                      {
                        title: 'Obligatorio',
                        dataIndex: 'required',
                        sorter: (a, b) => a.required - b.required,
                        sortOrder:
                          sortOrder.field === 'required' && sortOrder.order,
                      },
                      {
                        title: 'Modelo',
                        dataIndex: 'model',
                        sorter: (a, b) => a.model - b.model,
                        sortOrder:
                          sortOrder.field === 'model' && sortOrder.order,
                      },
                      {
                        title: 'Placa',
                        dataIndex: 'licensePlate',
                        sorter: (a, b) =>
                          a.licensePlate.localeCompare(b.licensePlate),
                        sortOrder:
                          sortOrder.field === 'licensePlate' && sortOrder.order,
                      },
                      {
                        title: 'Inciso',
                        dataIndex: 'inciso',
                        sorter: (a, b) => a.inciso - b.inciso,
                        sortOrder:
                          sortOrder.field === 'inciso' && sortOrder.order,
                      },
                      {
                        title: 'Marca',
                        dataIndex: 'brand',
                        sorter: (a, b) => a.brand - b.brand,
                        sortOrder:
                          sortOrder.field === 'brand' && sortOrder.order,
                      },
                      {
                        title: 'Cliente2*',
                        dataIndex: 'client2',
                        sorter: (a, b) => a.client2.localeCompare(b.client2),
                        sortOrder:
                          sortOrder.field === 'client2' && sortOrder.order,
                      },
                      {
                        title: 'Beneficiario*',
                        dataIndex: 'beneficiary',
                        sorter: (a, b) =>
                          a.beneficiary.localeCompare(b.beneficiary),
                        sortOrder:
                          sortOrder.field === 'beneficiary' && sortOrder.order,
                      },
                      {
                        title: '',
                        render: renderAction,
                      },
                    ]}
                    pagination={{
                      current: form.offset,
                      pageSize: form.fetch,
                      total: dataMovimientoVehicles.total,
                    }}
                    onChange={onChangeTable}
                  />
                </>
              )}
            </>
          )}

          <FooterCar />
        </Card>
      </animated.div>

      <Modal
        title={modalItem && `Detalle de Vehículo ${modalItem.id}`}
        centered
        width={900}
        visible={modal}
        okButtonProps={{ style: { display: 'none' } }}
        onCancel={() => hideModal()}
        cancelText="Cerrar"
      >
        {dataAmisFlotillaDetalle && modalItem && (
          <>
            <h2>VIN : {modalItem.vin}</h2>

            <Row gutter="24" className="form-data mt-1">
              <Col span="12">
                <p>Cliente 2*</p>
                <Form labelAlign="left" labelCol={{ span: 10 }}>
                  <Form.Item label="Apellido Paterno">
                    <span>
                      {dataAmisFlotillaDetalle.client2.apellidoMaterno}
                    </span>
                  </Form.Item>
                  <Form.Item label="Apellido Materno">
                    <span>
                      {dataAmisFlotillaDetalle.client2.apellidoPaterno}
                    </span>
                  </Form.Item>
                  <Form.Item label="Nombre">
                    <span>{dataAmisFlotillaDetalle.client2.nombre}</span>
                  </Form.Item>
                  <Form.Item label="CURP">
                    <span>{dataAmisFlotillaDetalle.client2.curp}</span>
                  </Form.Item>
                  <Form.Item label="RFC">
                    <span>{dataAmisFlotillaDetalle.client2.rfc}</span>
                  </Form.Item>
                </Form>
              </Col>

              <Col span="12">
                <h4>Beneficiario *</h4>
                <Form labelAlign="left" labelCol={{ span: 10 }}>
                  <Form.Item label="Apellido Paterno">
                    <span>
                      {dataAmisFlotillaDetalle.beneficiary.apellidoPaterno}
                    </span>
                  </Form.Item>
                  <Form.Item label="Apellido Materno">
                    <span>
                      {dataAmisFlotillaDetalle.beneficiary.apellidoMaterno}
                    </span>
                  </Form.Item>
                  <Form.Item label="Nombre">
                    <span>{dataAmisFlotillaDetalle.beneficiary.nombre}</span>
                  </Form.Item>
                  <Form.Item label="CURP">
                    <span>{dataAmisFlotillaDetalle.beneficiary.curp}</span>
                  </Form.Item>
                  <Form.Item label="RFC">
                    <span>{dataAmisFlotillaDetalle.beneficiary.rfc}</span>
                  </Form.Item>
                </Form>
              </Col>
            </Row>

            <h4 className="mt-2 mb-4">Cobertura</h4>

            <Table
              className="mt-5"
              columns={[
                {
                  title: 'Id',
                  dataIndex: 'id',
                },
                {
                  title: 'Id Suma Asegurada',
                  dataIndex: 'insuranceSumId',
                  // eslint-disable-next-line react/display-name
                  render: e => <>{number(e)}</>,
                },
                {
                  title: 'Suma Asegurada',
                  dataIndex: 'insuranceSum',
                  // eslint-disable-next-line react/display-name
                  render: e => <>{number(e)}</>,
                },
                {
                  title: 'Estatus',
                  dataIndex: 'status',
                },
              ]}
              dataSource={dataAmisFlotillaDetalle.coverage}
            />
          </>
        )}
      </Modal>
    </>
  )
}
export default AmisDetail
