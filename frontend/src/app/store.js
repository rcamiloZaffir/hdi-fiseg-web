/**
 * Store de la aplicacion web
 * @module app/store
 */
import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import logger from 'redux-logger'
import reducers from '@reducers'

const initialState = {}
const isProd = process.env.REACT_APP_ENV === 'production'
/**
 * Store del aplicativo web
 */
const store = () => {
  return createStore(
    reducers,
    initialState,
    isProd
      ? composeWithDevTools(applyMiddleware(thunkMiddleware))
      : composeWithDevTools(applyMiddleware(thunkMiddleware, logger))
  )
}

export default store
