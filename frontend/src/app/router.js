/**
 * Router de la aplicacion web
 * @module app/router
 */
/* eslint-disable no-restricted-syntax */
import React, { useEffect } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { TransitionGroup, CSSTransition } from 'react-transition-group'
import { v4 as uuidv4 } from 'uuid'
import uris from '@utils/uris'
import NotFound from '@pages/NotFound'
import Forbidden from '@pages/Forbidden'
import { getPermission } from '@actions/permission'
import { isAuthenticated } from '@utils'

/**
 * Render de url de permisos web
 * @param {String} urlPermission Nombre de la url para permisos
 */
const routesReader = urlPermission => {
  const routes = Object.entries(uris)
  const routesArray = []

  const addMenu = routeObj => {
    const key = uuidv4()
    const CurrentComponent = () => <routeObj.component key={key} />
    let MyComponent

    // private page
    if (routeObj.private) {
      if (isAuthenticated()) {
        MyComponent = CurrentComponent
      } else {
        // 403 Forbiden
        MyComponent = Forbidden
      }
    } else if (routeObj.private === false) {
      // public page
      MyComponent = CurrentComponent
    }

    if (urlPermission.includes(routeObj.uri)) {
      const element = (
        <Route key={routeObj.uri} exact path={routeObj.uri}>
          {({ match }) => (
            <CSSTransition
              in={match != null}
              timeout={300}
              classNames="page"
              unmountOnExit
            >
              <div className="page">
                <TransitionGroup>
                  <MyComponent />
                </TransitionGroup>
              </div>
            </CSSTransition>
          )}
        </Route>
      )
      routesArray.push(element)
    }
  }

  routes.map(item => {
    if (item.length > 1) {
      const itemMain = item[1]
      for (const itemSub in itemMain) {
        if (itemMain.hasOwnProperty(itemSub)) {
          if (itemMain[itemSub].hasOwnProperty('uri')) {
            const routeObj = itemMain[itemSub]
            addMenu(routeObj)
          }

          if (itemMain[itemSub].hasOwnProperty('children')) {
            const itemMaineChildren = itemMain[itemSub].children
            for (const i in itemMaineChildren) {
              if (itemMaineChildren[i].hasOwnProperty('uri')) {
                const routeObj = itemMaineChildren[i]
                addMenu(routeObj)
              }
            }
          }
        }
      }
    }
    return true
  })

  return routesArray
}

/**
 * Router de permisos
 */
const router = () => {
  const dispatch = useDispatch()
  const { url } = useSelector(state => state.permission)

  useEffect(() => {
    dispatch(getPermission())
  }, [])

  return (
    <Switch>
      {routesReader(url)}

      <Route exact path="/">
        {isAuthenticated() ? (
          <Redirect to="/dashboard" />
        ) : (
          <Redirect to="/login" />
        )}
      </Route>

      <Route exact path="/login">
        {isAuthenticated() ? (
          <Redirect to="/dashboard" />
        ) : (
          <Redirect to="/login" />
        )}
      </Route>

      {url.length > 0 && (
        <Route path="*">
          <NotFound />
        </Route>
      )}
    </Switch>
  )
}

export default router
