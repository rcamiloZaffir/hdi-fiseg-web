/**
 * Aplicacion web
 * @module app
 */
import React from 'react'
import { BrowserRouter as RRouter } from 'react-router-dom'
// import { HashRouter as RRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { IntlProvider } from 'react-intl'
import { ConfigProvider } from 'antd'
import esEs from 'antd/es/locale/es_ES'
import moment from 'moment'
import 'moment/locale/es'
import es from '@utils/translations/es.json'
import Portal from '@pages/Portal'
import initializeStore from './store'
import Router from './router'

moment.locale('es')

const store = initializeStore()

/**
 * Constructor de la aplicacion web App
 */
function App() {
  return (
    <Provider store={store}>
      <IntlProvider locale="es" messages={es}>
        <RRouter>
          <ConfigProvider locale={esEs}>
            <Portal>
              <Router />
            </Portal>
          </ConfigProvider>
        </RRouter>
      </IntlProvider>
    </Provider>
  )
}

export default App
