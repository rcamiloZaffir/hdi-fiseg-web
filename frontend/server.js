const express = require('express')
const bodyParser = require('body-parser')
const { createProxyMiddleware } = require('http-proxy-middleware')
const path = require('path')
const axios = require('axios')

const {
  API,
  API_PROXY_SECURITY_SERVICE,
  API_SERVICE_WRAPPER,
  PORTAL_ROLE,
} = process.env

const URL = {
  TokenAutentificacion: `${API_PROXY_SECURITY_SERVICE}/TokenAutentificacion`,
  Login: `${API_SERVICE_WRAPPER}/Login`,
  ObtenerPerfil: `${API_SERVICE_WRAPPER}/ObtenerPerfil`,
}

const app = express()

app.use(
  '/api',
  createProxyMiddleware({
    target: API,
    pathRewrite: { '^/api': '' },
    logLevel: 'debug',
  })
)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.post('/jwt/login', async (req, res) => {
  const { username, password } = req.body

  // try {
  axios
    .post(URL.TokenAutentificacion, {
      usuario: username,
      usuarioPwd: password,
      nIntentos: 0,
    })
    .then(tokenAutentificacion => {
      if (tokenAutentificacion) {
        console.info(`OK: ${URL.TokenAutentificacion}`)

        axios
          .post(URL.Login, {
            Usuario: username,
            Password: password,
            NumIntentosLogin: 0,
            HostGuid: '',
          })
          .then(login => {
            if (login && login.data.toString().match(PORTAL_ROLE)) {
              console.info(`OK: ${URL.Login}`)

              axios
                .get(`${URL.ObtenerPerfil}?login=${username}`, {
                  login: username,
                })
                .then(obtenerPerfil => {
                  if (obtenerPerfil && !obtenerPerfil.data.Error) {
                    console.info(`OK: ${URL.ObtenerPerfil}`)

                    res.status(200).send({
                      token: tokenAutentificacion.data.token,
                      username: obtenerPerfil.data.Objeto.NombreUsuario,
                      email: obtenerPerfil.data.Objeto.Mail,
                    })
                  } else {
                    console.error(
                      `Error : ${URL.ObtenerPerfil}?login=${username}`
                    )

                    res.status(401).send({
                      message: 'Error al obtener el email del usuario',
                    })
                  }
                })
                .catch(err => {
                  console.error(
                    'Error',
                    err,
                    URL.ObtenerPerfil,
                    JSON.stringify({
                      login: username,
                    })
                  )
                  res.status(404).send({
                    message:
                      'Error al intentar acceder a los servicios de email del usuario',
                  })
                })
            } else {
              console.error(
                `Error : ${URL.Login}            
                  Usuario: ${username},
                  Password: ${password},
                  NumIntentosLogin: 0,
                  HostGuid: '',
                `
              )
              // console.error('Error Response:', login)

              res.status(401).send({
                message: 'No se tiene permisos para ingresar al sistema',
              })
            }
          })
          .catch(err => {
            // console.log(err)
            console.error(
              'Error',
              err,
              URL.Login,
              JSON.stringify({
                Usuario: username,
                Password: password,
                NumIntentosLogin: 0,
                HostGuid: '',
              })
            )
            res.status(404).send({
              message:
                'Error al intentar acceder a los servicios de Rol del usuario',
            })
          })
      } else {
        console.error(`
            Error : ${URL.TokenAutentificacion}
              usuario: ${username}
              usuarioPwd: ${password}
              nIntentos: 0
            `)
        console.error('Error Response:', tokenAutentificacion)

        res.status(401).send({
          message: 'Usuario o contraseña incorrectos',
        })
      }
    })
    .catch(err => {
      console.error(
        'Error',
        err,
        URL.TokenAutentificacion,
        JSON.stringify({
          usuario: username,
          usuarioPwd: password,
          nIntentos: 0,
        })
      )
      res.status(404).send({
        message: 'Error al intentar acceder a los servicios de login',
      })
    })
})

app.get('/jwt/logout', (req, res) => {
  res.clearCookie('token')
  res.clearCookie('username')
  res.clearCookie('email')
  res.redirect(302, '/')
})

/**
 * Html
 */
app.use(express.static('build'))
app.use((req, res) => res.sendFile(path.join(__dirname, 'build', 'index.html')))

app.listen(5000)
