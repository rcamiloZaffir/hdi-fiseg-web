const sonarqubeScanner = require('sonarqube-scanner')

sonarqubeScanner(
  {
    serverUrl: 'http://45.55.109.255:9000',
    options: {
      'sonar.projectName': 'HDI-fiseg-web',
      'sonar.projectVersion': '1.0.0',
      'sonar.sources': 'src',
      // 'sonar.tests': 'src/__test__',
      // 'sonar.test.inclusions':
      //   'src/**/*.spec.js,src/**/*.spec.jsx,src/**/*.test.js,src/**/*.test.jsx,server/**/*.spec.js,server/**/*.test.js',
      'sonar.exclusions':
        'src/__test__/**,src/__test__/index.js,src/app/index.js,src/index.js',
      'sonar.sourceEncoding': 'UTF-8',
      'sonar.javascript.file.suffixes': '.js,.jsx',
      'sonar.javascript.lcov.reportPaths': 'coverage/lcov.info',
    },
  },
  () => {
    /* */
  }
)
