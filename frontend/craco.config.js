const CracoLessPlugin = require('craco-less')
const fs = require('fs')
const path = require('path')
const lessToJS = require('less-vars-to-js')
const CracoAlias = require('craco-alias')
// const dotenv = require('dotenv').config()

const themeVariables = lessToJS(
  fs.readFileSync(
    path.resolve(__dirname, './src/styles/globalVars.less'),
    'utf8'
  )
)

module.exports = {
  eslint: {
    enable: false,
  },
  jest: {
    testEnvironment: 'node',
    babel: {
      addPresets: true /* (default value) */,
      addPlugins: true /* (default value) */,
    },
    configure: jestConfig => {
      return jestConfig
    },
    // timeout: 60000,
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: themeVariables,
            javascriptEnabled: true,
          },
        },
      },
    },
    {
      plugin: CracoAlias,
      options: {
        aliases: {
          '@actions': './src/actions',
          '@components': './src/components',
          '@reducers': './src/reducers',
          '@pages': './src/pages',
          '@utils': './src/utils',
        },
      },
    },
  ],
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers':
        'X-Requested-With, content-type, Authorization',
    },
    proxy: {
      '/api': {
        target: 'http://localhost:5000/api',
        pathRewrite: { '^/api': '' },
      },
      '/jwt': {
        target: 'http://localhost:5000/jwt',
        pathRewrite: { '^/jwt': '' },
      },
    },
  },
}
